﻿using System.Collections.Generic;

namespace Project.Data.Entities
{
    public class Category : AuditableEntity
    {
        public string Name { get; set; }

        public int? ParentId { get; set; }
    }

    public static class CategoryData
    {
        public static List<Category> Data =>
            new List<Category>()
            {
                new Category
                {
                    Name = "Elektornika"
                },
                new Category
                {
                    Name = "Dom"
                },
                new Category
                {
                    Name = "Dziecko"
                },
                new Category
                {
                    Name = "Zdrowie i uroda"
                },
                new Category
                {
                    Name = "Sport"
                },
                new Category
                {
                    Name = "Fotografia",
                    ParentId = 1
                },
                new Category
                {
                    Name = "Telewizory",
                    ParentId = 1
                },              
                new Category
                {
                    Name = "Aparaty Cyfrowe",
                    ParentId = 6
                },
                new Category
                {
                    Name = "Obiektywy",
                    ParentId = 6
                },
                new Category
                {
                    Name = "Samsung",
                    ParentId = 7
                },
                new Category
                {
                    Name = "Lg",
                    ParentId = 7
                },
                new Category
                {
                    Name = "Sony",
                    ParentId = 7
                },
                new Category
                {
                    Name = "Meble",
                    ParentId = 2
                },
                new Category
                {
                    Name = "Oświetlenie",
                    ParentId = 2
                }
            };
    }
}
