﻿using System.Collections.Generic;

namespace Project.Data.Entities
{
    public class OrderItem : AuditableEntity
    {
        public int OrderId { get; set; }

        public virtual Order Order { get; set; }

        public int ProductId { get; set; }

        public virtual Product Product { get; set; }

        public decimal Price { get; set; }

        public int Count { get; set; }

    }

    public static class OrderItemData
    {
        public static List<OrderItem> Data =>
            new List<OrderItem>()
            {
                new OrderItem
                {
                    OrderId = 1,
                    ProductId = 1,
                    Price = 1999,
                    Count =3,
                    AddUserId = 1
                },
                new OrderItem
                {
                    OrderId = 1,
                    ProductId = 3,
                    Price = 1896,
                    Count =3,
                    AddUserId = 1
                },
                new OrderItem
                {
                    OrderId = 1,
                    ProductId = 11,
                    Price = 54,
                    Count =20,
                    AddUserId = 1
                }
            };
    }
}
