﻿using System;

namespace Project.Data.Entities
{
    public interface IAuditableEntity
    {
        int? AddUserId { get; set; }

        DateTime InsertTime { get; set; }

        int? ChangeUserId { get; set; }

        DateTime? ChangeTime { get; set; }
    }
}
