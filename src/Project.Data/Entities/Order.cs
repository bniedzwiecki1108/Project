﻿using System.Collections.Generic;

namespace Project.Data.Entities
{
    public class Order : AuditableEntity
    {

    }

    public static class OrderData
    {
        public static List<Order> Data =>
            new List<Order>()
            {
                new Order
                {
                    AddUserId = 1
                }
            };
    }
}
