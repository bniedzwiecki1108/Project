﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Project.Data.Entities
{
    public enum UserStatusEnum
    {
        [Description("Aktywny")]
        [Display(Name = "Aktywny")]
        Active = 1,

        [Description("Nieaktywny")]
        [Display(Name = "Nieaktywny")]
        NonActive = 2,

        [Description("Zablokowany")]
        [Display(Name = "Zablokowany")]
        Blocked = 3
    }

    public class UserStatus : DictionaryEntity
    {
    }
}
