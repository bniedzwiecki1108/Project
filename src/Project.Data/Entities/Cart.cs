﻿using System;

namespace Project.Data.Entities
{
    public class Cart : BaseEntity
    {
        public Cart()
        {
            InsertTime = DateTime.Now;
        }

        public int ProductId { get; set; }

        public virtual Product Product { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }

        public int Count { get; set; }

        public DateTime InsertTime { get; set; }

        public bool IsRealize { get; set; }
    }
}
