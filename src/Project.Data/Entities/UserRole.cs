﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Project.Data.Entities
{
    public enum UserRoleEnum
    {
        [Description("Administrator")]
        [Display(Name = "Administrator")]
        Admin = 1,

        [Description("Użytkownik")]
        [Display(Name = "Użytkownik")]
        User = 2
    }

    public class UserRole : DictionaryEntity
    {

    }
}
