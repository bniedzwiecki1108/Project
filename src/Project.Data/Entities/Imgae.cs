﻿using System.Collections.Generic;
using System.Linq;

namespace Project.Data.Entities
{
    public class Image : AuditableEntity
    {
        public string Path { get; set; }

        public string PathResize { get; set; }

        public string Name { get; set; }

        public string Extension { get; set; }

        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
    }

    public static class ImageData
    {
        public static List<Image> Data =>
            new List<Image>
            {
#if DEBUG
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "ecc8382e-5f2d-412d-ac0b-b272e78abff3.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "ecc8382e-5f2d-412d-ac0b-b272e78abff3.jpg",
                    Name = "ecc8382e-5f2d-412d-ac0b-b272e78abff3",
                    Extension = ".jpg",
                    ProductId = 1
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "dc26a1b6-bac5-486b-b149-bb8baadb3952.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "dc26a1b6-bac5-486b-b149-bb8baadb3952.jpg",
                    Name = "dc26a1b6-bac5-486b-b149-bb8baadb3952",
                    Extension = ".jpg",
                    ProductId = 2
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "d73e3df0-6c72-4e3c-8038-cd1c6a7b003a.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "d73e3df0-6c72-4e3c-8038-cd1c6a7b003a.jpg",
                    Name = "d73e3df0-6c72-4e3c-8038-cd1c6a7b003a",
                    Extension = ".jpg",
                    ProductId = 3
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "ce1be2f2-b582-4e47-a92a-6fe9f9c287e2.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "ce1be2f2-b582-4e47-a92a-6fe9f9c287e2.jpg",
                    Name = "ce1be2f2-b582-4e47-a92a-6fe9f9c287e2",
                    Extension = ".jpg",
                    ProductId = 4
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "6eb0e028-4923-4dfc-b192-ecb5ca31a1aa.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "6eb0e028-4923-4dfc-b192-ecb5ca31a1aa.jpg",
                    Name = "6eb0e028-4923-4dfc-b192-ecb5ca31a1aa",
                    Extension = ".jpg",
                    ProductId = 5
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "57422f7b-29cb-49d0-8c5b-a2b0922dc44a.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "57422f7b-29cb-49d0-8c5b-a2b0922dc44a.jpg",
                    Name = "57422f7b-29cb-49d0-8c5b-a2b0922dc44a",
                    Extension = ".jpg",
                    ProductId = 6
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "5fcb7ef2-6a21-419e-8e98-8d12046d7a64.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "5fcb7ef2-6a21-419e-8e98-8d12046d7a64.jpg",
                    Name = "5fcb7ef2-6a21-419e-8e98-8d12046d7a64",
                    Extension = ".jpg",
                    ProductId = 7
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "65129fc5-e0e8-492d-9e1f-6bd361444b70.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "65129fc5-e0e8-492d-9e1f-6bd361444b70.jpg",
                    Name = "65129fc5-e0e8-492d-9e1f-6bd361444b70",
                    Extension = ".jpg",
                    ProductId = 8
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "1e2d4724-2122-4093-b491-8e5a9e1352ba.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "1e2d4724-2122-4093-b491-8e5a9e1352ba.jpg",
                    Name = "1e2d4724-2122-4093-b491-8e5a9e1352ba",
                    Extension = ".jpg",
                    ProductId = 9
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "9112b616-8c50-4c7b-96e4-85b94507a615.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "9112b616-8c50-4c7b-96e4-85b94507a615.jpg",
                    Name = "9112b616-8c50-4c7b-96e4-85b94507a615",
                    Extension = ".jpg",
                    ProductId = 10
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "fa6aadd1-5459-4697-b856-d9bab1f1125e.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "fa6aadd1-5459-4697-b856-d9bab1f1125e.jpg",
                    Name = "fa6aadd1-5459-4697-b856-d9bab1f1125e",
                    Extension = ".jpg",
                    ProductId = 11
                },
                new Image
                {
                    Path = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePath").Value + "5ecedd25-3fc7-4230-b172-26366ab57330.jpg",
                    PathResize = ConfigurationData.Data.FirstOrDefault(k => k.Key == "ImagePathResize").Value + "5ecedd25-3fc7-4230-b172-26366ab57330.jpg",
                    Name = "5ecedd25-3fc7-4230-b172-26366ab57330",
                    Extension = ".jpg",
                    ProductId = 12
                }
            };
#endif
    }
}
