﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Data.Entities
{
    public class Product : AuditableEntity
    {
        [MaxLength(50)]
        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public int Count { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public ICollection<Image> Images { get; set; }
    }

    public static class ProductData
    {
        public static List<Product> Data =>
            new List<Product>
            {
#if DEBUG
                new Product
                {
                   Name = "Telewizor Samsung UE58NU7172 58'' LCD 4K HDR Smart",
                   Price = 1999,
                   Description = "Samsung UE55NU7172UXXH to 55-calowy telewizor o częstotliwości odświeżania obrazu 1300 (Hz), wyposażony w wyświetlacz LED, pracujący w rozdzielczości Ultra HD (3840 x 2160 px). Urządzenie posiada funkcję Smart TV, wbudowany moduł WiFi, 3 złącza HDMI oraz 2 złącza USB. Produkt mieści się w klasie energetycznej A.",
                   Count = 12,
                   CategoryId =10,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "Telewizor 65 Samsung QE65Q8FNA QLED 4K HDR",
                   Price = 3299,
                   Description = "Cztery słowa - ponad miliard odcieni kolorów. Oglądaj swoje ulubione programy i filmy o najbardziej realistycznych, dokładnych i żywych kolorach. Zobacz wszystkie szczegóły kolorystyczne każdej sceny, tak jakbyś był częścią akcji.    * Telewizory QLED zostały zweryfikowane przez światowej sławy stowarzyszenie zajmujące się testowaniem i certyfikacją, Verband Deutscher Elektrotechniker (VDE), ze względu na jego zdolność do wytwarzania 100-procentowego natężenia (głębi) kolorów.",
                   Count = 23,
                   CategoryId =10,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "Smart TV 55'' Samsung UE55ES6540 Full HD 3D WiFi",
                   Price = 1896,
                   Description = "Przedmiotem aukcji jest 55-calowy telewizor z technologią aktywnego 3D  Smart TV Samsung UE55ES6540  Telewizor posiada delikatne mikroryski na powierzchni obudowy i podstawy.",
                   Count = 34,
                   CategoryId = 10,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "LG OLED 55 SMART TV OLED55B7V 4K UHD HDR WIFI",
                   Price = 2536,
                   Description = "Przedmiotem aukcji jest 55-calowy telewizor typu OLED  Smart TV LG OLED55C8  Telewizor ma delikatnie wygiętą ramkę w prawym dolnym rogu (zdjęcie na samym dole aukcji). Jest to wada czysto estetyczna i nie wpływa na jakość wyświetlanego obrazu.",
                   Count = 23,
                   CategoryId = 11,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "Sony DSC-HX60 + 32GB + ładowarka + aku NP-BX1",
                   Price = 999,
                   Description = "Aparat cyfrowy Sony DSC-HX60  + akcesoria o łącznej wartości 97 zł  Sprzedawany aparat pochodzi z oficjalnej, polskiej sieci dystrybucji",
                   Count = 35,
                   CategoryId = 8,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "Aparat Nikon D3500+18-55 VR Nikkor Obiektyw",
                   Price = 1681 ,
                   Description = "Zastosowana w lustrzance D3500 matryca formatu DX o dużej rozdzielczości 24,2 mln pikseli rejestruje pełne szczegółów zdjęcia i filmy Full HD, nawet przy słabym oświetleniu. Dodanie do tego charakterystyk optycznych obiektywu NIKKOR daje zestaw, który pozwala tworzyć artystyczne portrety z rozmytym tłem. Z łatwością. Radość z prawdziwej fotografii w chwilach wyjątkowych i na co dzień. Kompaktowa lustrzanka D3500 łączy wysoką jakość wykonania z doskonałymi parametrami. Bezproblemowa obsługa spektakularnie ułatwia rejestrowanie na zdjęciach ważnych chwil i dzielenie się nimi.",
                   Count = 39,
                   CategoryId = 8,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "Szafa Przesuwna RICO 204 z lustrem + półki",
                   Price = 499,
                   Description = "Szafa wykonana jest z wysokiej jakości płyty laminowanej o grubości 16 mm w wielu wersjach kolorystycznych do wyboru w dalszej części aukcji.  W szafie zastosowany został bardzo solidny system drzwi przesuwnych (aluminiowe prowadnice + rolki na łożyskach) dzięki któremu drzwi płynnie, lekko i bez żadnego problemu się przesuwają.",
                   Count = 12,
                   CategoryId = 13,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "Meble kuchenne Zestaw mebli kuchennych z BLATEM",
                   Price = 539,
                   Description = "Zestaw mebli kuchennych posiada blat  Zestaw wykonany z wysokogatunkowej płyty meblowej typ DE COR posiadającej fakturę naturalnego drewna  Meble w paczkach do samodzielnego montażu  Meble posiadają instrukcję montażową w języku polskim oraz wszystkie niezbędne akcesoria do prawidłowego montażu szafek  Możliwość ustalenia kierunku otwierania drzwi w lewo lub w prawo  Każda z szafek dolnych za wyjątkiem zlewozmywakowej posiada odrębny blat roboczy, co pozwala na DOWOLNE ZESTAWIENIE KOLEJNOŚCI POSZCZEGÓLNYCH ELEMENTÓW ZESTAWU  Głębokość szafek dolnych / głębokość korpusu: 60cm / 45cm",
                   Count = 5,
                   CategoryId = 13,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "LAMPA SUFITOWA WISZĄCA W STYLU LOFT E27 Z DRUTU",
                   Price = 33,
                   Description = "Przedmiotem sprzedaży jest:  Designerska lampa sufitowa wisząca zaprojektowana w nowoczesnym stylu.  Nr kat: 802-Z1 LUXOLAR  Na tej aukcji kupujesz bezpośrednio od polskiego producenta - produkt Polski oryginalny.",
                   Count = 100 ,
                   CategoryId = 14,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "LEGO MINECRAFT Walka w Kresie 21151",
                   Price = 64,
                   Description = "Przygotuj się do ostatecznej walki w stylu Minecraft™ — przyda Ci się mikstura, Perła Endu i zaklęty miecz. Zniszcz Kryształ Kresu, pokonaj Endermana i wygraj walkę z ziejącym ognistymi kulami Smokiem Kresu! Z tym zestawem LEGO® Minecraft możesz na własnej skórze doświadczyć przygód ze świata Minecraft. Łącząc go z innymi modelami z serii, zbudujesz własne uniwersum LEGO Minecraft. W zestawie znajdziesz minifigurkę pogromcy smoków (nowość na styczeń 2019 r.) oraz kod, który można wymienić na takiego samego skina w grze online Minecraft Bedrock Editio",
                   Count = 22,
                   CategoryId =3,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "A Men (746) Perfumy 50ml",
                   Price = 54,
                   Description = "Nuta głowy to nuty zapachowe, które wyraźnie wyczuwamy jako pierwsze, a które odparowują najszybciej, są wyczuwalne od kilkunastu minut do godziny.    Nuta serca to sam środek zapachu perfum, to ta nuta najlepiej określa całość kompozycji i nadaje perfumom charakteru, utrzymuje się na skórze do kilku godzin.",
                   Count = 200,
                   CategoryId = 4,
                   AddUserId = 2
                },
                new Product
                {
                   Name = "PIĘKNY BIAŁY ROWER MIEJSKI 28 SHIMANO 7 BIEGÓW HIT",
                   Price = 559,
                   Description = "Rower miejski GERMAN 28 7 BIEGÓW to propozycja polskiego producenta. Wykonany w modnym stylu retro, idealnie wpisuje się w trend nadchodzącego sezonu. Rama 18 sugerowana jest dla osób o wzroście 156-180 cm. Rower wyposażony jest w 7 biegów SHIMANO TOURNEY, 2 hamulce v-brake, wzmacniane obręcze kół oraz  siodełko, chwyty i opony.",
                   Count = 124,
                   CategoryId = 5,
                   AddUserId = 2
                }
            };
#endif
    }
}
