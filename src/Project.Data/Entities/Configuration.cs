﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Data.Entities
{
    public class Configuration : AuditableEntity
    {
        [MaxLength(100)]
        public string Key { get; set; }

        [MaxLength(100)]
        public string Value { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }
    }

    public static class ConfigurationData
    {
        public static List<Configuration> Data =>
            new List<Configuration>()
            {
#if DEBUG
                new Configuration
                {
                   Key = "Salt",
                   Value = "dc5ecce2-7442-4bce-9b52-faf64358d6b5",
                   Description = "Sól do hasła"
                },
                new Configuration
                {
                    Key = "ImagePath",
                    Value =
                        System.AppDomain.CurrentDomain.BaseDirectory
                            .Replace(@"Project.Data\bin\Debug\", @"Project.Web\")
                            .Replace(@"Project.IntegrationTests\bin\Debug", @"Project.Web\")
                            + @"Content\Image\Products\",
                    Description = "Lokalizacja zdjęć"
                },
                new Configuration
                {
                    Key = "ImagePathResize",
                    Value =
                        System.AppDomain.CurrentDomain.BaseDirectory
                            .Replace(@"Project.Data\bin\Debug\", @"Project.Web\")
                            .Replace(@"Project.IntegrationTests\bin\Debug", @"Project.Web\")
                            + @"Content\Image\Products\Resize\",
                    Description = "Lokalizacja zdjęć"
                }
#endif
            };
    }
}
