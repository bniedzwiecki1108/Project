﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Data.Entities
{
    public class DictionaryEntity : BaseEntity
    {
        public DictionaryEntity()
        {
            InsertTime = DateTime.Now;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override int Id { get; set; }

        [Index("IX_DictionaryEntityNameUnique", IsUnique = true)]
        [MaxLength(50)]
        public string Name { get; set; }
                
        public DateTime InsertTime { get; set; }

        public DateTime? ChangeTime { get; set; }
    }
}
