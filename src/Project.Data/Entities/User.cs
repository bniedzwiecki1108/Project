﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Data.Entities
{
    public class User : AuditableEntity
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        [Index("IX_UserEmailUnique", IsUnique = true)]
        [MaxLength(200)]
        public string Email { get; set; }

        public string Password { get; set; }

        public int UserRoleId { get; set; }

        public virtual UserRole UserRole { get; set; }

        public int UserStatusId { get; set; }

        public virtual UserStatus UserStatus { get; set; }
    }

    public class UserProfile : AutoMapperProfile
    {
        public UserProfile()
        {
            CreateMap<UserModel, User>()
                .ForMember(ui => (UserRoleEnum)ui.UserRoleId, opt => opt.MapFrom(u => u.UserRole));
        }
    }

    public static class UserData
    {
        public static List<User> Data =>
            new List<User> {
#if DEBUG
                new User{
                    Name = "Użytkownik",
                    Surname = "Uzytkownik",
                    Email = "uzytkownik@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Bartosz",
                    Surname = "Niedżwiecki",
                    Email = "email@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.Admin,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Użytkownik1",
                    Surname = "Uzytkownik1",
                    Email = "uzytkownik1@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Użytkownik2",
                    Surname = "Uzytkownik2",
                    Email = "uzytkownik2@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Użytkownik3",
                    Surname = "Uzytkownik3",
                    Email = "uzytkownik3@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Użytkownik4",
                    Surname = "Uzytkownik4",
                    Email = "uzytkownik4@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Użytkownik5",
                    Surname = "Uzytkownik5",
                    Email = "uzytkownik5@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Użytkownik6",
                    Surname = "Uzytkownik6",
                    Email = "uzytkownik6@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Użytkownik7",
                    Surname = "Uzytkownik7",
                    Email = "uzytkownik7@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Użytkownik8",
                    Surname = "Uzytkownik8",
                    Email = "uzytkownik8@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Użytkownik9",
                    Surname = "Uzytkownik9",
                    Email = "uzytkownik9@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                }
                ,new User{
                    Name = "Użytkownik10",
                    Surname = "Uzytkownik10",
                    Email = "uzytkownik10@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                },
                new User{
                    Name = "Użytkownik11",
                    Surname = "Uzytkownik11",
                    Email = "uzytkownik11@wp.pl",
                    Password = "KojaF5unmddn1ZC76l9KZA==", //pass
                    UserRoleId = (int)UserRoleEnum.User,
                    UserStatusId = (int)UserStatusEnum.Active
                }
#endif
            };
    }
}
