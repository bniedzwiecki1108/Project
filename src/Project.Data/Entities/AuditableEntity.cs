﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project.Data.Entities
{
    public abstract class AuditableEntity : BaseEntity, IAuditableEntity
    {
        public AuditableEntity()
        {
            InsertTime = DateTime.Now;
            IsDeleted = false;
        }
        
        [ScaffoldColumn(false)]
        public int? AddUserId { get; set; }

        public virtual User AddUser { get; set; }

        public DateTime InsertTime { get; set; }
        
        [ScaffoldColumn(false)]        
        public int? ChangeUserId { get; set; }

        public virtual User ChangeUser { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? ChangeTime { get; set; }

        [ScaffoldColumn(false)]
        public bool IsDeleted { get; set; }

    }
}
