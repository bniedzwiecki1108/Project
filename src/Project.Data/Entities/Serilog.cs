﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Data.Entities
{
    public class Serilog : BaseEntity
    {
        public string Message { get; set; }

        public string MessageTemplate { get; set; }

        public string Level { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Exception { get; set; }

        [Column(TypeName = "xml")]
        public string Properties { get; set; }

        public int? UserId { get; set; }

        public virtual User User { get; set; }
    }
}
