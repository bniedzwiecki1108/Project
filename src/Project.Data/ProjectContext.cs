﻿using Project.Data.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Project.Data
{
    public class ProjectContext : DbContext
    {
        public ProjectContext() : base("ProjectContext") =>          
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ProjectContext, Migrations.Configuration>());

        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<UserStatus> UserStatuses { get; set; }

        public virtual DbSet<UserRole> UserRoles { get; set; }

        public virtual DbSet<Serilog> Serilogs { get; set; }

        public virtual DbSet<Configuration> Configurations { get; set; }

        public virtual DbSet<Category> Categories { get; set; }

        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<Image> Image { get; set; }

        public virtual DbSet<Cart> Carts { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

        public virtual DbSet<OrderItem> OrderItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<User>()
              .HasOptional(u => u.AddUser)
              .WithMany()
              .HasForeignKey(u => u.AddUserId);
            modelBuilder.Entity<User>()
                .HasOptional(u => u.ChangeUser)
                .WithMany()
                .HasForeignKey(u => u.ChangeUserId);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }
    }
}
