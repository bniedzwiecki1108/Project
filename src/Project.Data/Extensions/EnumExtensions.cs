﻿using Project.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Project.Data.Extensions
{
    public static class EnumExtensions
    {
        public static void SeedEnumValues<T, TEnum>(this IDbSet<T> dbSet, Func<TEnum, int> converter)
            where T : DictionaryEntity, new()
            where TEnum : struct =>
                ToList<TEnum>()
                .Select(value =>
                    new T
                    {
                        Id = converter(value),
                        Name = value.GetDescription()
                    })
                .OrderBy(e => e.Id)
                .ToList()
                .ForEach(instance => dbSet.AddOrUpdate(e => e.Id, instance));

        public static string GetDescription<TEnum>(this TEnum item) =>
            item
                .GetType()
                .GetField(item.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .Cast<DescriptionAttribute>()
                .FirstOrDefault()?.Description ?? string.Empty;
        
        private static List<TEnum> ToList<TEnum>() where TEnum : struct =>
            Enum.GetValues(typeof(TEnum))
                .Cast<TEnum>()
                .ToList();

        public static int ToInt<TEnum>(this TEnum item) =>
            Convert.ToInt32(item);

    }
}
