﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Entities;
using System;
using System.Linq;

namespace Project.Data.Models
{
    public class CartModel
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public int UserId { get; set; }

        public int Count { get; set; }

        public DateTime InsertTime { get; set; }

        public string PathResize { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }
    }

    public class CartModelProfile : AutoMapperProfile
    {
        public CartModelProfile()
        {
            CreateMap<Cart, CartModel>()
                .ForMember(desc => desc.PathResize, src => src.MapFrom(i => i
                                                                            .Product
                                                                            .Images
                                                                            .Select(a => a.PathResize)
                                                                            .FirstOrDefault()
                                                                            ))
                .ForMember(desc => desc.Name, src => src.MapFrom(i => i
                                                                    .Product
                                                                    .Name
                                                                    ))
                .ForMember(desc => desc.Price, src => src.MapFrom(i => i
                                                                    .Product
                                                                    .Price
                                                                    ));
        }
    }
}
