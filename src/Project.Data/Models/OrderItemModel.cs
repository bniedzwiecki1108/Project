﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Entities;
using System;
using System.Linq;

namespace Project.Data.Models
{
    public class OrderItemModel
    {
        public int OrderId { get; set; }        

        public int ProductId { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Count { get; set; }

        public DateTime InsertTime { get; set; }

        public string PathResize { get; set; }

        public int AddUserId { get; set; }
    }

    public class OrderModelProfile : AutoMapperProfile
    {
        public OrderModelProfile()
        {
            CreateMap<OrderItem, OrderItemModel>()
                .ForMember(desc => desc.Name, src => src.MapFrom(i => i
                                                                        .Product
                                                                        .Name))
                .ForMember(desc => desc.PathResize, src => src.MapFrom(i => i
                                                                            .Product
                                                                            .Images
                                                                            .Select(a => a.PathResize)
                                                                            .FirstOrDefault()));
                }
    }
}
