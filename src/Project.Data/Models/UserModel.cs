﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Entities;
using System;

namespace Project.Data.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public UserRoleEnum UserRole { get; set; }

        public UserStatusEnum UserStatus { get; set; }

        public DateTime InsertTime { get; set; }

        public DateTime? ChangeTime { get; set; }
    }

    public class UserModelProfile : AutoMapperProfile
    {
        public UserModelProfile()
        {
            CreateMap<User, UserModel>()
                .ForMember(desc => desc.UserRole, src => src.MapFrom(u => (UserRoleEnum)u.UserRoleId))
                .ForMember(desc => desc.UserStatus, src => src.MapFrom(u => (UserStatusEnum)u.UserStatusId));
        }
    }
}
