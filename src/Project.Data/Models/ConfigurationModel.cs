﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Entities;

namespace Project.Data.Models
{
    public class ConfigurationModel
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public string Description { get; set; }
    }

    public class ConfigurationModelProfile : AutoMapperProfile
    {
        public ConfigurationModelProfile()
        {
            CreateMap<Configuration, ConfigurationModel>();
        }
    }
}
