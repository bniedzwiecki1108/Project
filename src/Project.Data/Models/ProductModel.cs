﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Entities;
using System.Linq;

namespace Project.Data.Models
{
    public class ProductModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public int Count { get; set; }

        public int CategoryId { get; set; }

        public string Path { get; set; }

        public string PathResize { get; set; }
    }

    public class ProductModelProfile : AutoMapperProfile
    {
        public ProductModelProfile()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(desc => desc.Path, src => src.MapFrom(i => i
                                                                        .Images
                                                                        .Select(a => a.Path)
                                                                        .FirstOrDefault()
                                                                        ))
               .ForMember(desc => desc.PathResize, src => src.MapFrom(i => i
                                                                            .Images
                                                                            .Select(a => a.PathResize)
                                                                            .FirstOrDefault()
                                                                            ));
        }
    }
}
