﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Entities;

namespace Project.Data.Models
{
    public class CategoryModel
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int? ParentId { get; set; }
    }

    public class CategoryModelProfile : AutoMapperProfile
    {
        public CategoryModelProfile()
        {
            CreateMap<Category, CategoryModel>();
        }
    }
}
