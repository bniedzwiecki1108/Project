namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ParentId = c.Int(),
                        AddUserId = c.Int(),
                        InsertTime = c.DateTime(nullable: false),
                        ChangeUserId = c.Int(),
                        ChangeTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.AddUserId)
                .ForeignKey("dbo.User", t => t.ChangeUserId)
                .Index(t => t.AddUserId)
                .Index(t => t.ChangeUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Category", "ChangeUserId", "dbo.User");
            DropForeignKey("dbo.Category", "AddUserId", "dbo.User");
            DropIndex("dbo.Category", new[] { "ChangeUserId" });
            DropIndex("dbo.Category", new[] { "AddUserId" });
            DropTable("dbo.Category");
        }
    }
}
