namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderAndOrderItemTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Count = c.Int(nullable: false),
                        AddUserId = c.Int(),
                        InsertTime = c.DateTime(nullable: false),
                        ChangeUserId = c.Int(),
                        ChangeTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.AddUserId)
                .ForeignKey("dbo.User", t => t.ChangeUserId)
                .ForeignKey("dbo.Order", t => t.OrderId)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .Index(t => t.OrderId)
                .Index(t => t.ProductId)
                .Index(t => t.AddUserId)
                .Index(t => t.ChangeUserId);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AddUserId = c.Int(),
                        InsertTime = c.DateTime(nullable: false),
                        ChangeUserId = c.Int(),
                        ChangeTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.AddUserId)
                .ForeignKey("dbo.User", t => t.ChangeUserId)
                .Index(t => t.AddUserId)
                .Index(t => t.ChangeUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderItem", "ProductId", "dbo.Product");
            DropForeignKey("dbo.OrderItem", "OrderId", "dbo.Order");
            DropForeignKey("dbo.Order", "ChangeUserId", "dbo.User");
            DropForeignKey("dbo.Order", "AddUserId", "dbo.User");
            DropForeignKey("dbo.OrderItem", "ChangeUserId", "dbo.User");
            DropForeignKey("dbo.OrderItem", "AddUserId", "dbo.User");
            DropIndex("dbo.Order", new[] { "ChangeUserId" });
            DropIndex("dbo.Order", new[] { "AddUserId" });
            DropIndex("dbo.OrderItem", new[] { "ChangeUserId" });
            DropIndex("dbo.OrderItem", new[] { "AddUserId" });
            DropIndex("dbo.OrderItem", new[] { "ProductId" });
            DropIndex("dbo.OrderItem", new[] { "OrderId" });
            DropTable("dbo.Order");
            DropTable("dbo.OrderItem");
        }
    }
}
