namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSerilogTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Serilog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        MessageTemplate = c.String(),
                        Level = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                        Exception = c.String(),
                        Properties = c.String(storeType: "xml"),
                        UserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Serilog", "UserId", "dbo.User");
            DropIndex("dbo.Serilog", new[] { "UserId" });
            DropTable("dbo.Serilog");
        }
    }
}
