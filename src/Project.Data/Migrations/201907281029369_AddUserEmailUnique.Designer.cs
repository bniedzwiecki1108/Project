// <auto-generated />
namespace Project.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddUserEmailUnique : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddUserEmailUnique));
        
        string IMigrationMetadata.Id
        {
            get { return "201907281029369_AddUserEmailUnique"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
