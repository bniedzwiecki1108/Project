namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImageTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Image",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Path = c.String(),
                        PathResize = c.String(),
                        Name = c.String(),
                        Extension = c.String(),
                        ProductId = c.Int(nullable: false),
                        AddUserId = c.Int(),
                        InsertTime = c.DateTime(nullable: false),
                        ChangeUserId = c.Int(),
                        ChangeTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.AddUserId)
                .ForeignKey("dbo.User", t => t.ChangeUserId)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.AddUserId)
                .Index(t => t.ChangeUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Image", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Image", "ChangeUserId", "dbo.User");
            DropForeignKey("dbo.Image", "AddUserId", "dbo.User");
            DropIndex("dbo.Image", new[] { "ChangeUserId" });
            DropIndex("dbo.Image", new[] { "AddUserId" });
            DropIndex("dbo.Image", new[] { "ProductId" });
            DropTable("dbo.Image");
        }
    }
}
