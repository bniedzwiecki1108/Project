using Project.Data.Entities;
using Project.Data.Extensions;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Project.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ProjectContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;            
        }

        protected override void Seed(ProjectContext context)
        {
            context.UserStatuses.SeedEnumValues<UserStatus, UserStatusEnum>(@enum => (int)@enum);
            context.UserRoles.SeedEnumValues<UserRole, UserRoleEnum>(@enum => (int)@enum);
            UserData.Data
                .ForEach(u =>
                {
                    context.Users.AddOrUpdate(o => o.Email, u);
                });
            context.SaveChanges();

            ConfigurationData.Data
                .ForEach(c =>
                {
                    context.Configurations.AddOrUpdate(o => o.Key, c);
                });
            context.SaveChanges();

            CategoryData.Data
                .ForEach(c =>
                {
                    context.Categories.AddOrUpdate(o => o.Name, c);
                });
            context.SaveChanges();

            ProductData.Data
                .ForEach(c =>
                {
                    context.Products.AddOrUpdate(o => o.Name, c);
                });
            context.SaveChanges();

            ImageData.Data
                .ForEach(c =>
                {
                    context.Image.AddOrUpdate(o => o.Name, c);
                });
            context.SaveChanges();

            if (!context.Orders.Where(o => o.Id == 1).Any())
            {
                OrderData.Data
                    .ForEach(c =>
                    {
                        context.Orders.AddOrUpdate(o => o.Id, c);
                    });
                context.SaveChanges();
            }

            if (!context.OrderItems.Where(o => o.Id == 1).Any())
            {
                OrderItemData.Data
                .ForEach(c =>
                {
                    context.OrderItems.AddOrUpdate(o => new { o.ProductId, o.Price, o.Count }, c);
                });
                context.SaveChanges();
            }

            base.Seed(context);
        }
    }
}
