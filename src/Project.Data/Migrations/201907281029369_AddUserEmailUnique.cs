namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserEmailUnique : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.User", "Email", c => c.String(maxLength: 200));
            CreateIndex("dbo.User", "Email", unique: true, name: "IX_UserEmailUnique");
        }
        
        public override void Down()
        {
            DropIndex("dbo.User", "IX_UserEmailUnique");
            AlterColumn("dbo.User", "Email", c => c.String());
        }
    }
}
