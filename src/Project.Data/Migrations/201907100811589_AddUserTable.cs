namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(maxLength: 50),
                        InsertTime = c.DateTime(nullable: false),
                        ChangeTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_DictionaryEntityNameUnique");
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        UserRoleId = c.Int(nullable: false),
                        UserStatusId = c.Int(nullable: false),
                        AddUserId = c.Int(),
                        InsertTime = c.DateTime(nullable: false),
                        ChangeUserId = c.Int(),
                        ChangeTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.AddUserId)
                .ForeignKey("dbo.User", t => t.ChangeUserId)
                .ForeignKey("dbo.UserRole", t => t.UserRoleId, cascadeDelete: true)
                .ForeignKey("dbo.UserStatus", t => t.UserStatusId, cascadeDelete: true)
                .Index(t => t.UserRoleId)
                .Index(t => t.UserStatusId)
                .Index(t => t.AddUserId)
                .Index(t => t.ChangeUserId);
            
            CreateTable(
                "dbo.UserStatus",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(maxLength: 50),
                        InsertTime = c.DateTime(nullable: false),
                        ChangeTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "IX_DictionaryEntityNameUnique");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.User", "UserStatusId", "dbo.UserStatus");
            DropForeignKey("dbo.User", "UserRoleId", "dbo.UserRole");
            DropForeignKey("dbo.User", "ChangeUserId", "dbo.User");
            DropForeignKey("dbo.User", "AddUserId", "dbo.User");
            DropIndex("dbo.UserStatus", "IX_DictionaryEntityNameUnique");
            DropIndex("dbo.User", new[] { "ChangeUserId" });
            DropIndex("dbo.User", new[] { "AddUserId" });
            DropIndex("dbo.User", new[] { "UserStatusId" });
            DropIndex("dbo.User", new[] { "UserRoleId" });
            DropIndex("dbo.UserRole", "IX_DictionaryEntityNameUnique");
            DropTable("dbo.UserStatus");
            DropTable("dbo.User");
            DropTable("dbo.UserRole");
        }
    }
}
