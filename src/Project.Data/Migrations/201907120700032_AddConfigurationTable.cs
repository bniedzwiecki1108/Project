namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConfigurationTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Configuration",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(maxLength: 100),
                        Value = c.String(maxLength: 100),
                        Description = c.String(maxLength: 500),
                        AddUserId = c.Int(),
                        InsertTime = c.DateTime(nullable: false),
                        ChangeUserId = c.Int(),
                        ChangeTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.AddUserId)
                .ForeignKey("dbo.User", t => t.ChangeUserId)
                .Index(t => t.AddUserId)
                .Index(t => t.ChangeUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Configuration", "ChangeUserId", "dbo.User");
            DropForeignKey("dbo.Configuration", "AddUserId", "dbo.User");
            DropIndex("dbo.Configuration", new[] { "ChangeUserId" });
            DropIndex("dbo.Configuration", new[] { "AddUserId" });
            DropTable("dbo.Configuration");
        }
    }
}
