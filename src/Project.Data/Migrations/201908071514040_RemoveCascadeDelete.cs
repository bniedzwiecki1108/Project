namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveCascadeDelete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.User", "UserRoleId", "dbo.UserRole");
            DropForeignKey("dbo.User", "UserStatusId", "dbo.UserStatus");
            DropForeignKey("dbo.Image", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Product", "CategoryId", "dbo.Category");
            AddForeignKey("dbo.User", "UserRoleId", "dbo.UserRole", "Id");
            AddForeignKey("dbo.User", "UserStatusId", "dbo.UserStatus", "Id");
            AddForeignKey("dbo.Image", "ProductId", "dbo.Product", "Id");
            AddForeignKey("dbo.Product", "CategoryId", "dbo.Category", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Product", "CategoryId", "dbo.Category");
            DropForeignKey("dbo.Image", "ProductId", "dbo.Product");
            DropForeignKey("dbo.User", "UserStatusId", "dbo.UserStatus");
            DropForeignKey("dbo.User", "UserRoleId", "dbo.UserRole");
            AddForeignKey("dbo.Product", "CategoryId", "dbo.Category", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Image", "ProductId", "dbo.Product", "Id", cascadeDelete: true);
            AddForeignKey("dbo.User", "UserStatusId", "dbo.UserStatus", "Id", cascadeDelete: true);
            AddForeignKey("dbo.User", "UserRoleId", "dbo.UserRole", "Id", cascadeDelete: true);
        }
    }
}
