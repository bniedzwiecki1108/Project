namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(),
                        Count = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        AddUserId = c.Int(),
                        InsertTime = c.DateTime(nullable: false),
                        ChangeUserId = c.Int(),
                        ChangeTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.AddUserId)
                .ForeignKey("dbo.Category", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.ChangeUserId)
                .Index(t => t.CategoryId)
                .Index(t => t.AddUserId)
                .Index(t => t.ChangeUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Product", "ChangeUserId", "dbo.User");
            DropForeignKey("dbo.Product", "CategoryId", "dbo.Category");
            DropForeignKey("dbo.Product", "AddUserId", "dbo.User");
            DropIndex("dbo.Product", new[] { "ChangeUserId" });
            DropIndex("dbo.Product", new[] { "AddUserId" });
            DropIndex("dbo.Product", new[] { "CategoryId" });
            DropTable("dbo.Product");
        }
    }
}
