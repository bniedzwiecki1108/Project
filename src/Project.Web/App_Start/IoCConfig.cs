﻿using Autofac;
using Autofac.Integration.Mvc;
using Project.Service.Helper.Serilog;
using Project.Service.Infrastructure.Authorization;
using Project.Service.Settings;
using Project.Web.Helpers;
using Project.Web.Infrastructure.Modules;
using System.Web.Mvc;

namespace Project.Web.App_Start
{
    public class IoCConfig
    {
        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder
                .RegisterControllers(typeof(MvcApplication).Assembly)
                .PropertiesAutowired();
            builder
                .RegisterModule(new RepositoryModule());
            builder
                .RegisterModule(new ServiceModule());
            builder
                .RegisterModule(new EFModule());
            builder
                .RegisterModule(new AutoMapperModule());

            builder
                .RegisterType<SettingsManager>()
                .As<ISettingsManager>()
                .SingleInstance();

            builder
                .RegisterType<AuthenticationManager>()
                .As<IAuthenticationManager>()
                .SingleInstance();

            builder
                .RegisterType<CashManagerHelper>()
                .As<ICashManagerHelper>()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<LogService>()
                .As<ILogService>()
                .InstancePerLifetimeScope();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            return container;
        }
    }
}