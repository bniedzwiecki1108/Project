﻿using FluentValidation.Mvc;
using Project.Data.Entities;
using Project.Data.Extensions;
using Project.Service.Helper.Serilog;
using Project.Service.Infrastructure.Authorization;
using Project.Web.App_Start;
using Project.Web.Helpers;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Project.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var container = IoCConfig.BuildContainer();
            DependencyResolver.Current.GetService<ICashManagerHelper>().SetCashe();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            FluentValidationModelValidatorProvider.Configure();
        }

        protected void Application_Error()
        {
            var ex = Server.GetLastError();
            var _logService = DependencyResolver.Current.GetService<ILogService>();
            _logService.Logger.Error(ex, "ApplicationError");
        }

        protected void FormsAuthentication_OnAuthenticate(Object sender, FormsAuthenticationEventArgs e)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                var _authenticationManager = DependencyResolver.Current.GetService<IAuthenticationManager>();
                string roles = ((UserRoleEnum)_authenticationManager.GetUserData().UserRoleId).GetDescription();

                e.User = new GenericPrincipal(
                  new GenericIdentity(username, "Forms"), roles.Split(';'));
            }
        }
    }
}
