﻿using Project.Service.Helper.Serilog;
using Project.Service.Services;
using Project.Web.TemporaryMessage;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;
        private readonly ILogService _logService;
        private readonly ICartService _cartService;

        public OrderController(
            IOrderService orderService,
            ILogService logService,
            ICartService cartService)
        {
            _orderService = orderService;
            _logService = logService;
            _cartService = cartService;
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create()
        {
            var cardItems =
                _cartService
                    .GetCartItems();

            if (cardItems.Count == 0)
                SetTemporaryMessage(OrderMessage.CartEmpty, TemporaryType.Warning);

            else
            {
                int id = _orderService.Create(cardItems);

                SetTemporaryMessage(OrderMessage.AddOrder, TemporaryType.Succes);
               _logService.Logger.Information("Zrealizowano zamówienie o id : {@}", id);             
            }

            return RedirectToAction("Index", "Product");
        }
    }
}