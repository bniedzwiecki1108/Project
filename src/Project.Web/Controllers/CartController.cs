﻿using AutoMapper;
using Project.Data.Models;
using Project.Service.Services;
using Project.Web.TemporaryMessage;
using Project.Web.ViewModels.Cart;
using Project.Web.ViewModels.Product;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class CartController : BaseController
    {
        private readonly ICartService _cartService;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        [HttpGet]
        [Authorize]
        public PartialViewResult Index()
        {
            var data =
                new CartIndexViewModel
                {
                    Carts =
                        Mapper.Map<List<CartIndexRowViewModel>>(
                            _cartService
                                .GetCartItems())
                };

            return PartialView("_Index", data.Carts);
        }


        [HttpPost]
        [Authorize]
        public ActionResult Create(ProductDetailsViewModel productDetailsViewModel)
        {
            var productModel = Mapper.Map<ProductModel>(productDetailsViewModel);

            _cartService.Create(productModel);

            SetTemporaryMessage(CartMessage.CartAdded, TemporaryType.Succes);

            return RedirectToAction("Index", "Product");
        }


        [HttpPost]
        [Authorize]
        public PartialViewResult Delete (int id)
        {
            _cartService.Delete(id);

            return Index();
        }
    }
}