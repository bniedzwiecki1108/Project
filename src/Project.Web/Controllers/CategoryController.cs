﻿using Project.Data.Models;
using Project.Service.Services;
using Project.Web.ViewModels.Category;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var data = GetTreeCategory();
            return PartialView("_Index",data);
        }

        public List<CategoryIndexViewModel> GetTreeCategory() =>
            FillRecursive(_categoryService.GetCategories(), null);

        private List<CategoryIndexViewModel> FillRecursive(List<CategoryModel> categories, int? parentId = null) =>
            categories
                .Where(x => x.ParentId.Equals(parentId))
                .Select(item => new CategoryIndexViewModel
                {
                    Name = item.Name,
                    Id = item.Id.Value,
                    Children = FillRecursive(categories, item.Id)
                })
                .ToList();
    }
}