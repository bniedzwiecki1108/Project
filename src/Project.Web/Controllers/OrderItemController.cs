﻿using AutoMapper;
using Project.Data.Entities;
using Project.Service.Infrastructure.Authorization;
using Project.Service.Services;
using Project.Web.ViewModels.OrderItem;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class OrderItemController : BaseController
    {
        private readonly IOrderItemService _orderItemService;
        private readonly IAuthenticationManager _authenticationManager;

        public OrderItemController(
            IOrderItemService orderItemService,
            IAuthenticationManager authenticationManager)
        {
            _orderItemService = orderItemService;
            _authenticationManager = authenticationManager;
        }

        [Authorize]
        [HttpGet]
        public ActionResult Index() =>
            View();

        [Authorize]
        [HttpGet]
        public ActionResult IndexGrid()
        {
            var user = _authenticationManager.GetUserData();

            var data =
                new OrderItemIndexViewModel
                {
                    Orders = UserRoleEnum.User == (UserRoleEnum)user.UserRoleId
                        ? Mapper.Map<List<OrderItemIndexRowViewModel>>(
                                _orderItemService
                                    .GetOrdersBy(user.Id))
                        : Mapper.Map<List<OrderItemIndexRowViewModel>>(
                                _orderItemService
                                    .GetOrders())
                };

            return PartialView("_IndexGrid", data.Orders);
        }
    }
}