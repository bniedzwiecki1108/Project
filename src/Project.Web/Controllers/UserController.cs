﻿using AutoMapper;
using Project.Data.Entities;
using Project.Data.Extensions;
using Project.Data.Models;
using Project.Service.Helper.Serilog;
using Project.Service.Infrastructure.Authorization;
using Project.Service.Services;
using Project.Web.Infrastructure.Role;
using Project.Web.TemporaryMessage;
using Project.Web.ViewModels.User;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly ILogService _logService;

        public UserController(
            IUserService userService,
            IAuthenticationManager authenticationManager,
            ILogService logService)
        {
            _userService = userService;
            _authenticationManager = authenticationManager;
            _logService = logService;
        }

        [Authorize(Roles = AppRoles.Admin)]
        [HttpGet]
        public ActionResult Index() =>
            View();

        [Authorize(Roles = AppRoles.Admin)]
        [HttpGet]
        public ActionResult IndexGrid()
        {
            var users = new UserIndexViewModel
            {
                Users =
                   Mapper.Map<List<UserIndexRowViewModel>>(
                       _userService
                           .GetUsers())
                           .ToList()
            };

            return PartialView("_IndexGrid", users.Users);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Register() =>
            PartialView("_Register");

        public JsonResult IsEmailExists(string email) =>
            Json(
                !_userService
                    .GetEmails()
                    .Any(x => x.Contains(email)),
                JsonRequestBehavior.AllowGet);

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserRegisterViewModel userRegisterViewModel)
        {
            if (ModelState.IsValid)
            {
                var userModelToCreate = Mapper.Map<UserModel>(userRegisterViewModel);
                userModelToCreate.Password = _authenticationManager.HashPassword(userModelToCreate.Password);
                var user = _userService.Create(userModelToCreate);
                SetTemporaryMessage(UserMessage.RegistredSuccessfully, TemporaryType.Succes);
                _logService.Logger.Information("Zarejestrowano uzytkownika : {@user}", user);

                return RedirectToAction("Index", "Product");
            }
            SetTemporaryMessage(UserMessage.RegistrationError, TemporaryType.Danger);
            _logService.Logger.Warning("Nie udało się zarejestrować użytkownika : {@userRegisterViewModel}", userRegisterViewModel);

            return RedirectToAction("Index", "Product");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login() =>
            PartialView("_Login");

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLoginViewModel userLoginViewModel)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.GetUserBy(userLoginViewModel.Email);
                if (user != null)
                {
                    if (user.UserStatusId == UserStatusEnum.NonActive.ToInt())
                    {
                        SetTemporaryMessage(UserMessage.InactiveAccount, TemporaryType.Warning);
                        return RedirectToAction("Index", "Product");
                    }

                    if (user.UserStatusId == UserStatusEnum.Blocked.ToInt())
                    {
                        SetTemporaryMessage(UserMessage.BlockedAccount, TemporaryType.Warning);
                        return RedirectToAction("Index", "Product");
                    }

                    if (_authenticationManager.IsPasswordConfirmed(
                            userLoginViewModel.Password,
                            user.Password))
                    {
                        _authenticationManager.SetAuthenticationToken(user);
                        return RedirectToAction("Index", "Product");
                    }
                }
            }
            SetTemporaryMessage(UserMessage.IncorrectLoginOrPassword, TemporaryType.Warning);
            return RedirectToAction("Index", "Product");
        }

        [HttpGet]
        [Authorize]
        public ActionResult Logout()
        {
            _authenticationManager.Logout();

            return RedirectToAction("Index", "Product");
        }

        [HttpGet]
        [Authorize]
        public ActionResult Edit(int? id)
        {
            id = _authenticationManager.GetLoggedUserId();

            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user =
                Mapper.Map<UserEditViewModel>(
                    _userService
                        .GetUserBy(id.Value));

            return View(user);
        }

        [HttpPost]
        public ActionResult Edit(UserEditViewModel userEditViewModel)
        {
            if (!ModelState.IsValid)
                return View(userEditViewModel);

            var userModelToEdit = Mapper.Map<UserModel>(userEditViewModel);

            _userService
                .Edit(userModelToEdit);

            SetTemporaryMessage(UserMessage.UserUpdated, TemporaryType.Succes);
            _logService.Logger.Information("Zaktualizowano uzytkownika : {@user}", userModelToEdit);

            return View(userEditViewModel);
        }

        [HttpGet]
        [Authorize]
        public ActionResult EditByAdmin(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user =
                Mapper.Map<UserEditByAdminViewModel>(
                    _userService
                        .GetUserBy(id.Value));

            return PartialView("_EditByAdmin", user);
        }

        [HttpPost]
        public ActionResult EditByAdmin(UserEditByAdminViewModel userEditByAdminViewModel)
        {
            var userModelToEdit = Mapper.Map<UserModel>(userEditByAdminViewModel);

            _userService
                .EditByAdmin(userModelToEdit);

            SetTemporaryMessage(UserMessage.UserUpdated, TemporaryType.Succes);
            _logService.Logger.Information("Zaktualizowano uzytkownika : {@user}", userModelToEdit);

            return RedirectToAction("Index");
        }
    }
}