﻿using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {     
        }

        protected void SetTemporaryMessage(string temporaryMessage, string temporaryType)
        {
            TempData["TemporaryMessage"] = temporaryMessage;
            TempData["TemporaryType"] = temporaryType;
        }
    }

    public static class TemporaryType
    {
        public static string Danger = "alert alert-danger";
        public static string Succes = "alert alert-success";
        public static string Warning = "alert alert-warning";
    }
}