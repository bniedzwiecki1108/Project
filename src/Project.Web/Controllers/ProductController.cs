﻿using AutoMapper;
using Project.Data.Models;
using Project.Service.Helper.Serilog;
using Project.Service.Services;
using Project.Web.Infrastructure.Role;
using Project.Web.TemporaryMessage;
using Project.Web.ViewModels.Product;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class ProductController : BaseController
    {
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly ILogService _logService;

        public ProductController(
            ICategoryService categoryService,
            IProductService productService,
            ILogService logService)
        {
            _categoryService = categoryService;
            _productService = productService;
            _logService = logService;
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index(int? categoryId)
        {
            IEnumerable<ProductModel> products;

            if(categoryId == null)
                products =
                    _productService
                        .GetProducts();
            else
            {
                var categories =
                    _categoryService
                        .GetCategories();

                var childrenCategoriesId =
                    _categoryService
                        .GetChildrenCategoriesId(
                            categories,
                            categoryId.Value);

                childrenCategoriesId.Add(categoryId.Value);

                products =
                    _productService
                        .GetProductsBy(childrenCategoriesId);
            }

            var data = new ProductIndexViewModel
            {
                Products = Mapper.Map<List<ProductIndexRowViewModel>>(products)
            };

            return View(data.Products);
        }

        [Authorize(Roles = AppRoles.Admin)]
        [HttpGet]
        public ActionResult Create()
        {
            var data = new ProductCreateViewModel
            {
                Category = _categoryService.GetCategories()
            };

            data.Category.Insert(0, new CategoryModel
            {
                Id = null,
                Name = string.Empty
            });

            return View(data);
        }

        [HttpPost]
        public ActionResult Create(ProductCreateViewModel createViewModel, HttpPostedFileBase Image)
        {
            if (ModelState.IsValid)
            {
                var prodcutModel = Mapper.Map<ProductModel>(createViewModel);

                var product =
                    _productService
                        .Create(prodcutModel, Image);

                SetTemporaryMessage(ProductMessage.ProductAdded, TemporaryType.Succes);
                _logService.Logger.Information("Dodano product : {@product}", product);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var data =
                Mapper.Map<ProductDetailsViewModel>(
                    _productService
                        .GetProductBy(id.Value));

            return View(data);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Details(ProductDetailsViewModel productDetailsViewModel)
        {
            if (!ModelState.IsValid)
            {
                var data =
                Mapper.Map<ProductDetailsViewModel>(
                    _productService
                        .GetProductBy(productDetailsViewModel.Id));
                return View(data);

            }

            return View();
        }
    }
}