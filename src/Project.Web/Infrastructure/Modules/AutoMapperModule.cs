﻿using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using Project.Core.Infrastructure.AutoMapper;
using System;
using System.Reflection;

namespace Project.Web.Infrastructure.Modules
{
    public class AutoMapperModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            builder
                .RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies())
                .Where(t => t.GetCustomAttribute<InjectableAttribute>() != null)
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterInstance(new AutoMapperConfiguration().Configure()).As<IMapper>();

            builder.RegisterFilterProvider();

            AutoMapperInitalize();
        }

        private static void AutoMapperInitalize()
        {
#pragma warning disable CS0618
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfiles(Assembly.GetExecutingAssembly());
                cfg.CreateMissingTypeMaps = true;
                //cfg.ValidateInlineMaps = false;
            });
#pragma warning restore CS0618
        }
    }
}