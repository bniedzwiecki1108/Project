﻿namespace Project.Web.Infrastructure.Role
{
    public class AppRoles
    {
        public const string Admin = "Administrator";
        public const string User = "Użytkownik";
    }
}