﻿    $(document).ready(function () {
        $('#modalRegisterButton').click(function () {
            var url = $('#modalRegister').data('url');
            $.get(url, function (data) {
                $("#modalRegister").html(data).appendTo("body");
                $("#modalRegister").modal('show').appendTo("body");
            });
        });
    });