﻿    $(document).ready(function () {
        $('#modalLoginButton').click(function () {
            var url = $('#modalLogin').data('url');
            $.get(url, function (data) {
                $("#modalLogin").html(data).appendTo("body");
                $("#modalLogin").modal('show').appendTo("body");
            });
        });
    });