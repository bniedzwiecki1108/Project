﻿$(document).ready(function () {   
    $("#btnMinus").click(function () {
        var value = parseInt($("#BuyQuantity").val());
        $("#BuyQuantity").val(value - 1);
    });

    $("#btnPlus").click(function () {
        var value = parseInt($("#BuyQuantity").val());
        $("#BuyQuantity").val(value + 1);
    });
});