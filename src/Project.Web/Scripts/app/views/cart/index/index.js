﻿$(document).ready(function () {
    $('#modalCartButton').click(function () {
        var url = $('#modalCart').data('url');
        $.get(url, function (data) {
            $("#modalCart").html(data).appendTo("body");
            $("#modalCart").modal('show').appendTo("body");
        });
    });
});

$(document).delegate('.btn-danger', 'click', function (e) {
    var DeleteItem = parseInt($(this).closest('tr').prop('id'));
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Cart/Delete',
        data: "{ 'id':' " + DeleteItem + "' }",
        success: function (data) {
            $("#modalCart").html(data);            
        },
        error: function (data) {
            alert("Coś poszło nie tak");
        }
    });
    e.preventDefault();
});