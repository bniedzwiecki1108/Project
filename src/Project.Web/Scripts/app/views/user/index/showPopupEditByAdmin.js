﻿function showPopupEditByAdmin(id) {
    var url = $('#modalEditByAdmin').data('url') + "/" + id;
    $.get(url, function (data) {
        $("#modalEditByAdmin").html(data);
        $("#modalEditByAdmin").modal('show');
    });

    var myUrl = '@Url.Action("EditByAdmin", "User)/' + id;
    $.ajax.load(myUrl);
}