﻿using FluentValidation;
using FluentValidation.Attributes;
using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Models;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Project.Web.ViewModels.User
{
    [Validator(typeof(UserRegisterViewModelFluentValidator))]
    public class UserRegisterViewModel
    {
        [Display(Name = "Imię")]        
        public string Name { get; set; }

        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }

        [Remote("IsEmailExists", "User", ErrorMessage = "Email użytkownika jest zajęty")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Hasło")]
        public string Password { get; set; }
        
        [Display(Name = "Powtórz hasło")]
        public string ConfirmPassword { get; set; }
    }

    public class UserRegisterViewModelProfile : AutoMapperProfile
    {
        public UserRegisterViewModelProfile()
        {
            CreateMap<UserRegisterViewModel, UserModel>();
        }
    }

    public class UserRegisterViewModelFluentValidator : AbstractValidator<UserRegisterViewModel>
    {
        public UserRegisterViewModelFluentValidator()
        {
            RuleFor(r => r.Name)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");

            RuleFor(r => r.Surname)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");

            RuleFor(r => r.Email)
                .NotEmpty()
                .WithMessage("Pole jest wymagane")
                .EmailAddress()
                .WithMessage("Niepoprawny adres Email");

            RuleFor(r => r.Password)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");

            RuleFor(r => r.ConfirmPassword)
                .NotEmpty()
                .WithMessage("Pole jest wymagane")
                .Equal(r => r.Password)
                .WithMessage("Hasła są niepoprawne");
        }
    }
}