﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Entities;
using Project.Data.Extensions;
using Project.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Web.ViewModels.User
{
    public class UserIndexViewModel
    {
        public IEnumerable<UserIndexRowViewModel> Users { get; set; }
    }

    public class UserIndexRowViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Imię")]
        public string Name { get; set; }

        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Rola")]
        public string UserRole { get; set; }

        [Display(Name = "Status")]
        public string UserStatus { get; set; }

        [Display(Name = "Data Dodania")]
        public DateTime InsertTime { get; set; }
    }

    public class UserIndexRowViewModelProfile : AutoMapperProfile
    {       
        public UserIndexRowViewModelProfile()
        {
            CreateMap<UserModel, UserIndexRowViewModel>()
                .ForMember(desc => desc.UserRole, src => src.MapFrom(u => u.UserRole.GetDescription()))
                .ForMember(desc => desc.UserStatus, src => src.MapFrom(u => u.UserStatus.GetDescription()));
        }
    }
}