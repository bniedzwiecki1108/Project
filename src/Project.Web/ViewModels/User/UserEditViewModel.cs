﻿using FluentValidation;
using FluentValidation.Attributes;
using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace Project.Web.ViewModels.User
{
    [Validator(typeof(UserEditViewModelFluentValidator))]
    public class UserEditViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Imię")]
        public string Name { get; set; }

        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }
    }

    public class UserEditViewModelProfile : AutoMapperProfile
    {
        public UserEditViewModelProfile()
        {
            CreateMap<UserModel, UserEditViewModel>();

            CreateMap<UserEditViewModel, UserModel>();
        }
    }

    public class UserEditViewModelFluentValidator : AbstractValidator<UserEditViewModel>
    {
        public UserEditViewModelFluentValidator()
        {
            RuleFor(r => r.Name)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");

            RuleFor(r => r.Surname)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");
        }
    }
}