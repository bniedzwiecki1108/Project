﻿using FluentValidation;
using FluentValidation.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Project.Web.ViewModels.User
{
    [Validator(typeof(UserLoginViewModelFluentValidator))]
    public class UserLoginViewModel 
    {
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Hasło")]
        public string Password { get; set; }
    }

    public class UserLoginViewModelFluentValidator : AbstractValidator<UserLoginViewModel>
    {
        public UserLoginViewModelFluentValidator()
        {
            RuleFor(r => r.Email)
                .NotEmpty()
                .WithMessage("Pole jest wymagane")
                .EmailAddress()
                .WithMessage("Niepoprawny adress email");

            RuleFor(r => r.Password)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");
        }
    }
}