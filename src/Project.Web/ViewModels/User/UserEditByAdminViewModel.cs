﻿using FluentValidation;
using FluentValidation.Attributes;
using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Entities;
using Project.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace Project.Web.ViewModels.User
{
    [Validator(typeof(UserEditByAdminViewModelFluentValidator))]
    public class UserEditByAdminViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Imię")]
        public string Name { get; set; }

        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }

        [Display(Name = "Rola")]
        public UserRoleEnum UserRole { get; set; }

        [Display(Name = "Status")]
        public UserStatusEnum UserStatus { get; set; }
    }

    public class UserEditByAdminViewModelProfile : AutoMapperProfile
    {
        public UserEditByAdminViewModelProfile()
        {
            CreateMap<UserModel, UserEditByAdminViewModel>();

            CreateMap<UserEditByAdminViewModel, UserModel>();
        }
    }

    public class UserEditByAdminViewModelFluentValidator : AbstractValidator<UserEditByAdminViewModel>
    {
        public UserEditByAdminViewModelFluentValidator()
        {
            RuleFor(r => r.Name)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");

            RuleFor(r => r.Surname)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");
        }
    }
}