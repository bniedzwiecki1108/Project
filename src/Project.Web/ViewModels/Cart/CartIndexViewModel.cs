﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Web.ViewModels.Cart
{
    public class CartIndexViewModel
    {
        public IEnumerable<CartIndexRowViewModel> Carts { get; set; }
    }

    public class CartIndexRowViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Zdjęcie")]
        public string PathResize { get; set; }

        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Display(Name = "Cena")]
        public decimal Price { get; set; }

        [Display(Name = "Ilość")]
        public int Count { get; set; }
    }

    public class CartIndexViewModelProfile : AutoMapperProfile
    {
        public CartIndexViewModelProfile()
        {
            CreateMap<CartModel, CartIndexRowViewModel>();
        }
    }
}