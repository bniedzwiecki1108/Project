﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Models;
using System;

namespace Project.Web.ViewModels.Cart
{
    public class CartCreateModel
    {
        public int ProductId { get; set; }

        public int UserId { get; set; }

        public int Count { get; set; }

        public DateTime InsertTime { get; set; }
    }

    public class CartCreateModelProfile : AutoMapperProfile
    {
        public CartCreateModelProfile()
        {
            CreateMap<CartCreateModel, CartModel>();
        }
    }
}