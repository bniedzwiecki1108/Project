﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Models;
using System;
using System.Collections.Generic;

namespace Project.Web.ViewModels.OrderItem
{
    public class OrderItemIndexViewModel
    {
        public List<OrderItemIndexRowViewModel> Orders;
    }

    public class OrderItemIndexRowViewModel
    {
        public int OrderId { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Count { get; set; }

        public DateTime InsertTime { get; set; }

        public string PathResize { get; set; }
    }

    public class OrderItemIndexRowViewModelProfile : AutoMapperProfile
    {
        public OrderItemIndexRowViewModelProfile()
        {
            CreateMap<OrderItemModel, OrderItemIndexRowViewModel>();
        }
    }
}