﻿using System.Collections.Generic;

namespace Project.Web.ViewModels.Category
{
    public class CategoryIndexViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public CategoryIndexViewModel ParentCategory { get; set; }

        public List<CategoryIndexViewModel> Children { get; set; }
    }
}
