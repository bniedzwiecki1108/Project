﻿using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Models;
using System.Collections.Generic;

namespace Project.Web.ViewModels.Product
{
    public class ProductIndexViewModel
    {
        public IEnumerable<ProductIndexRowViewModel> Products { get; set; }
    }

    public class ProductIndexRowViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string PathResize { get; set; }
    }

    public class ProductIndexViewModelProfile : AutoMapperProfile
    {
        public ProductIndexViewModelProfile()
        {
            CreateMap<ProductModel, ProductIndexRowViewModel>();
        }
    }
}