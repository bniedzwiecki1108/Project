﻿using FluentValidation;
using FluentValidation.Attributes;
using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace Project.Web.ViewModels.Product
{
    [Validator(typeof(ProductDetailsViewModelFluentValidator))]
    public class ProductDetailsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [Display(Name = "Cena")]
        public decimal Price { get; set; }

        [Display(Name= "Opis")]
        public string Description { get; set; }
                
        public int Count { get; set; }

        public string Path { get; set; }

        [Display(Name = "Ilość")]
        public int BuyQuantity { get; set; } = 1;
    }

    public class ProductDetailsViewModelProfile : AutoMapperProfile
    {
        public ProductDetailsViewModelProfile()
        {
            CreateMap<ProductModel, ProductDetailsViewModel>();

            CreateMap<ProductDetailsViewModel, ProductModel>()
                .ForMember(desc => desc.Count, src => src.MapFrom(u => u.BuyQuantity));
        }
    }

    public class ProductDetailsViewModelFluentValidator : AbstractValidator<ProductDetailsViewModel>
    {
        public ProductDetailsViewModelFluentValidator()
        {
            RuleFor(r => r.BuyQuantity)
                .NotEmpty()
                .WithMessage("Pole jest wymagane")
                .GreaterThan(0)
                .WithMessage("Wartość musi być większa od zera");
        }
    }
}