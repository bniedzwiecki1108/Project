﻿using FluentValidation;
using FluentValidation.Attributes;
using Project.Core.Infrastructure.AutoMapper;
using Project.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.Web.ViewModels.Product
{
    [Validator(typeof(ProductCreateViewModelFluentValidator))]
    public class ProductCreateViewModel
    {
        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Pole musi być liczbą")]
        [Display(Name = "Cena")]
        public decimal Price { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Display(Name = "Zdjęcie")]
        public string Image { get; set; }

        [RegularExpression(@"^\d+$", ErrorMessage = "Pole musi być liczbą")]
        [Display(Name = "Ilość")]
        public int Count { get; set; }

        public int CategoryId { get; set; }

        [Display(Name = "Kategoria")]
        public List<CategoryModel> Category { get; set; }
    }

    public class ProductCreateViewModellProfile : AutoMapperProfile
    {
        public ProductCreateViewModellProfile()
        {
            CreateMap<ProductModel, ProductCreateViewModel>();
            CreateMap<ProductCreateViewModel, ProductModel>();
        }
    }

    public class ProductCreateViewModelFluentValidator : AbstractValidator<ProductCreateViewModel>
    {
        public ProductCreateViewModelFluentValidator()
        {
            RuleFor(r => r.Name)
                .NotEmpty()
                .WithMessage("Pole jest wymagane")
                .MaximumLength(50)
                .WithMessage("Pole może mieć maksymalnie 50 znaków");

            RuleFor(r => r.Price)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");

            RuleFor(r => r.Description)
               .NotEmpty()
               .WithMessage("Pole jest wymagane");

            RuleFor(r => r.Count)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");

            RuleFor(r => r.Image)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");

            RuleFor(r => r.CategoryId)
                .NotEmpty()
                .WithMessage("Pole jest wymagane");
        }
    }
}