﻿using Project.Service.Infrastructure.Authorization;
using Project.Service.Services;
using System.Linq;

namespace Project.Web.Helpers
{
    public interface ICashManagerHelper
    {
        void SetCashe();
    }

    public class CashManagerHelper : ICashManagerHelper
    {
        private readonly IConfigurationService _configurationService;
        private readonly IMemoryCasheService _memoryCasheService;

        public CashManagerHelper(
            IConfigurationService configurationService,
            IMemoryCasheService memoryCasheService)
        {
            _configurationService = configurationService;
            _memoryCasheService = memoryCasheService;
        }

        public void SetCashe()
        {
            var configurations = _configurationService.GetConfigurations().ToList();

            configurations.ForEach(c =>
            {
                _memoryCasheService.Set(c.Key, c.Value);
            });
        }
    }
}