﻿namespace Project.Web.TemporaryMessage
{
    public static class UserMessage
    {
        public static readonly string RegistredSuccessfully = "Zarejestrowano pomyślnie.";
        public static readonly string RegistrationError = "Nie udało się zarejestrować użytkownika.";
        public static readonly string InactiveAccount = "Nie możesz się zalogować. Twoje konto jest nieaktywne.";
        public static readonly string BlockedAccount = "Nie możesz się zalogować. Twoje konto jest zablokowane.";
        public static readonly string IncorrectLoginOrPassword = "Podałeś nieprawidłowy login lub hasło.";
        public static readonly string UserUpdated = "Pomyślnie zaktualizowano użytkownika.";
    }
}