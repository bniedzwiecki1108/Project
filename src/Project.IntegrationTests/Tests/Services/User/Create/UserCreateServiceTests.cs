﻿using Project.Data.Entities;
using Project.Data.Models;
using Project.IntegrationTests.Infrastructure;
using Shouldly;
using Xunit;

namespace Project.IntegrationTests.Tests.Services.User.Create
{
    public class UserCreateServiceTests : IntergrationTestBase 
    {
        [Fact]
        public void Create_OnCorrectUser_ShouldNotBeZero()
        {
            //Arrange
            var userModel = new UserModel
            {
                Name = "Bartosz",
                Surname = "Niedżwiecki",
                Email = "emailtest2@wp.pl",
                Password = "KojaF5unmddn1ZC76l9KZA==",
                UserRole = UserRoleEnum.Admin,
                UserStatus = UserStatusEnum.Active
            };

            //Act
            var result = _userService.Create(userModel);

            //Assert
            result.Id.ShouldNotBe(0);
        }

        [Fact]
        public void GetUsers_ShouldNotBeNull()
        {
            //Act
            var result = _userService.GetUsers();

            //Assert
            result.ShouldNotBeNull();
        }
    }
}
