﻿using Moq;
using Project.Data.Models;
using Project.IntegrationTests.Infrastructure;
using Shouldly;
using System.IO;
using System.Web;
using Xunit;

namespace Project.IntegrationTests.Tests.Services.Product.Create
{
    public class ProductCreateServiceTest : IntergrationTestBase
    {
        [Fact]
        public void Create_Product_ShouldBeCorrect()
        {
            // Arange
            var product = new ProductModel
            {
                Name = "test",
                Price = 1999,
                Description = "test",
                Count = 12,
                CategoryId = 10
            };

            byte[] mybytes = File.ReadAllBytes(_settingsManager.ImagePathResize + "ecc8382e-5f2d-412d-ac0b-b272e78abff3.jpg");
            var _stream = new MemoryStream(mybytes);

            var fileMock = new Mock<HttpPostedFileBase>();
            fileMock.Setup(x => x.InputStream).Returns(_stream);
            fileMock.Setup(x => x.ContentLength).Returns((int)_stream.Length);
            fileMock.Setup(x => x.FileName).Returns("testimage1.jpg");

            // Act
            var insertedProduct =
                _productService
                    .Create(product, fileMock.Object);

            var image =
                _imageService
                    .GetImageBy(insertedProduct.Id);

            File.Delete(image.PathResize);

            //Assert
            insertedProduct.Id.ShouldNotBe(0);
        }
    }
}