﻿using Project.IntegrationTests.Infrastructure;
using Project.Web.ViewModels.User;
using Shouldly;
using Xunit;

namespace Project.IntegrationTests.Tests.Controllers.User.Create
{
    public class UserCreateControllerTests : IntergrationTestBase
    {
        [Fact]
        public void Register_Password_ShouldBeCorrect()
        {
            //Arange
            var user = new UserRegisterViewModel
            {
                Name = "Bartosz",
                Surname = "Niedżwiecki",
                Email = "emailtest@wp.pl",
                Password = "pass",
                ConfirmPassword = "pass"
            };

            //Act
            _userController.Register(user);
            var insertedUser = _userService.GetUserBy(user.Email);

            //Result
            insertedUser.Password.ShouldBe("KojaF5unmddn1ZC76l9KZA==");
        }
    }
}
