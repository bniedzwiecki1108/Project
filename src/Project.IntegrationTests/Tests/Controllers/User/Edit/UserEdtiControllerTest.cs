﻿using Project.IntegrationTests.Infrastructure;
using Project.Web.ViewModels.User;
using Shouldly;
using Xunit;

namespace Project.IntegrationTests.Tests.Controllers.User.Edit
{
    public class UserEdtiControllerTest : IntergrationTestBase
    {
        [Fact]
        public void Edit_User_ShouldBeCorrent()
        {
            //Arange
            string name = "Tomek";
            string surname = "Kowalski";

            var user = new UserEditViewModel
            {
                Id = 2,
                Name = name,
                Surname = surname
            };

            //Act
            _userController.Edit(user);

            var updatedUser =
                _userService
                    .GetUserBy(user.Id);

            //Assert
            updatedUser.Name.ShouldBe(name);
            updatedUser.Surname.ShouldBe(surname);
        }
    }
}
