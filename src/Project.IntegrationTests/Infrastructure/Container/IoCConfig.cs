﻿using Autofac;
using Autofac.Integration.Mvc;
using Project.Core.UnitOfWork;
using Project.Data;
using Project.Service.Helper.Serilog;
using Project.Service.Infrastructure.Authorization;
using Project.Service.Settings;
using Project.Web;
using Project.Web.Helpers;
using Project.Web.Infrastructure.Modules;
using System.Data.Entity;

namespace Project.IntegrationTests.Infrastructure.Container
{
    public class IoCConfig
    {
        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder
                .RegisterControllers(typeof(MvcApplication).Assembly)
                .PropertiesAutowired();
            builder
                .RegisterModule(new RepositoryModule());

            builder
                .RegisterModule(new ServiceModule());

            builder
                .RegisterType(typeof(UnitOfWork))
                .As(typeof(IUnitOfWork))
                .InstancePerLifetimeScope();

            builder
                .RegisterType(typeof(ProjectContext))
                .As(typeof(DbContext))
                .InstancePerLifetimeScope();

            builder.
                RegisterModule(new AutoMapperModule());

            builder
                .RegisterType<SettingsManager>()
                .AsSelf()
                .AsImplementedInterfaces();

            builder
                .RegisterType<AuthenticationManager>()
                .As<IAuthenticationManager>()
                .SingleInstance();

            builder
                .RegisterType<CashManagerHelper>()
                .AsSelf()
                .AsImplementedInterfaces();

            builder
                .RegisterType<LogService>()
                .AsSelf()
                .AsImplementedInterfaces();

            var container = builder.Build();

            return container;
        }
    }
}
