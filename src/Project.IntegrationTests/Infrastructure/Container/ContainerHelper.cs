﻿using Autofac;
using Project.Web.Helpers;

namespace Project.IntegrationTests.Infrastructure.Container
{
    public static class ContainerHelper
    {
        public static IContainer Container;

        static ContainerHelper()
        {
            Container = IoCConfig.BuildContainer();
            Container.BeginLifetimeScope();
            Container.Resolve<CashManagerHelper>().SetCashe();
        }
    }
}
