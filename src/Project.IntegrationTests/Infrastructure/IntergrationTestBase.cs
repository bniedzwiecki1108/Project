﻿using Project.Service.Services;
using Project.Service.Settings;
using Project.Web.Controllers;
using System;
using Xunit;

namespace Project.IntegrationTests.Infrastructure
{
    [Collection("Sequential")]
    public class IntergrationTestBase : IDisposable
    {
        private readonly TestFixture _testFixture;
        protected readonly UserController _userController;
        protected readonly UserService _userService;
        protected readonly ProductService _productService;
        protected readonly SettingsManager _settingsManager;
        protected readonly ImageService _imageService;

        public IntergrationTestBase()
        {
            _testFixture = new TestFixture();
            _userController = _testFixture.GetInstance<UserController>();
            _userService = _testFixture.GetInstance<UserService>();
            _productService = _testFixture.GetInstance<ProductService>();
            _settingsManager = _testFixture.GetInstance<SettingsManager>();
            _imageService = _testFixture.GetInstance<ImageService>();
        }

        public void Dispose() =>
            _testFixture.Dispose();
    }
}
