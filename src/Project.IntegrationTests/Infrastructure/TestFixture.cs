﻿using Autofac;
using Project.IntegrationTests.Infrastructure.Container;
using System;
using System.Transactions;

namespace Project.IntegrationTests.Infrastructure
{
    public class TestFixture : IDisposable
    {
        private readonly TransactionScope scope;

        public readonly static IContainer Container = ContainerHelper.Container;

        public TestFixture() =>
            scope = new TransactionScope();

        public void Dispose() =>
            scope.Dispose();

        public T GetInstance<T>() where T : class =>
            Container.Resolve<T>();
    }
}
