﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Project.Core.Services;
using Project.Core.UnitOfWork;
using Project.Data.Entities;
using Project.Data.Extensions;
using Project.Data.Models;
using Project.Repositories.Repository;
using Project.Service.Infrastructure.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.Service.Services
{
    public interface IUserService : IEntityService<User>
    {
        User Create(UserModel userModel);

        UserModel GetUserBy(int id);

        IEnumerable<UserModel> GetUsers();

        User GetUserBy(string email);

        IEnumerable<string> GetEmails();

        void Edit(UserModel userModelToEdit);

        void EditByAdmin(UserModel userModelToEdit);
    }

    public class UserService : EntityService<User>, IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IAuthenticationManager _authenticationManager;

        public UserService(
            IUnitOfWork unitOfWork,
            IUserRepository userRepository,
            IMapper mapper,
            IAuthenticationManager authenticationManager)
                : base(unitOfWork, userRepository)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _mapper = mapper;
            _authenticationManager = authenticationManager;
        }

        public User Create(UserModel userModel)
        {
            var user = new User
            {
                Name = userModel.Name,
                Surname = userModel.Surname,
                Email = userModel.Email,
                Password = userModel.Password,
                UserRoleId = (int)UserRoleEnum.User,
                UserStatusId = (int)UserStatusEnum.Active
            };
            Create(user);

            return user;
        }

        public UserModel GetUserBy(int id) =>
            _userRepository
                .Queryable()
                .ProjectTo<UserModel>(_mapper.ConfigurationProvider)
                .Where(u => u.Id == id)
                .FirstOrDefault();

        public User GetUserBy(string email) =>
            Queryable()
                .Where(e => e.Email == email)
                .FirstOrDefault();

        public IEnumerable<UserModel> GetUsers() =>
            _userRepository
                .Queryable()
                .ProjectTo<UserModel>(_mapper.ConfigurationProvider)
                .ToList();

        public IEnumerable<string> GetEmails() =>
            Queryable()
                .Select(e => e.Email)
                .ToList();

        public void Edit(UserModel userModelToEdit)
        {
            var user = Find(userModelToEdit.Id);

            user.Name = userModelToEdit.Name;
            user.Surname = userModelToEdit.Surname;
            user.ChangeTime = DateTime.Now;
            user.ChangeUserId = _authenticationManager.GetLoggedUserId();

            Update(user);
        }

        public void EditByAdmin(UserModel userModelToEdit)
        {
            var user = Find(userModelToEdit.Id);

            user.Name = userModelToEdit.Name;
            user.Surname = userModelToEdit.Surname;
            user.UserRoleId = userModelToEdit.UserRole.ToInt();
            user.UserStatusId = userModelToEdit.UserStatus.ToInt();
            user.ChangeTime = DateTime.Now;
            user.ChangeUserId = _authenticationManager.GetLoggedUserId();

            Update(user);
        }
    }
}
