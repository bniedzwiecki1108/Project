﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Project.Core.Services;
using Project.Core.UnitOfWork;
using Project.Data.Entities;
using Project.Data.Models;
using Project.Repository.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Project.Service.Services
{
    public interface IOrderItemService : IEntityService<OrderItem>
    {
        List<OrderItemModel> GetOrdersBy(int userId);

        List<OrderItemModel> GetOrders();
    }

    public class OrderItemService : EntityService<OrderItem>, IOrderItemService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IMapper _mapper;

        public OrderItemService(
            IUnitOfWork unitOfWork,
            IOrderItemRepository orderItemRepository,
            IMapper mapper) : base(unitOfWork, orderItemRepository)
        {
            _unitOfWork = unitOfWork;
            _orderItemRepository = orderItemRepository;
            _mapper = mapper;
        }

        public List<OrderItemModel> GetOrders() =>
            _orderItemRepository
                .Queryable()
                .OrderByDescending(i => i.Id)
                .ProjectTo<OrderItemModel>(_mapper.ConfigurationProvider)
                .ToList();

        public List<OrderItemModel> GetOrdersBy(int userId) =>
            GetOrders()
                .Where(u => u.AddUserId == userId)
                .ToList();
    }
}
