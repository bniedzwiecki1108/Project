﻿using System.Collections.Generic;
using AutoMapper;
using Project.Core.Services;
using Project.Core.UnitOfWork;
using Project.Data.Entities;
using Project.Data.Models;
using Project.Repository.Repositories;
using Project.Service.Infrastructure.Authorization;

namespace Project.Service.Services
{
    public interface IOrderService : IEntityService<Order>
    {
        int Create(List<CartModel> cardItems);       
    }

    public class OrderService : EntityService<Order>, IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;
        private readonly ICartService _cartService;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IProductService _productService;

        public OrderService(
            IUnitOfWork unitOfWork,
            IOrderRepository orderRepository,
            IMapper mapper,
            ICartService cartService,
            IOrderItemRepository orderItemRepository,
            IAuthenticationManager authenticationManager,
            IProductService productService) : base(unitOfWork, orderRepository)
        {
            _unitOfWork = unitOfWork;
            _orderRepository = orderRepository;
            _mapper = mapper;
            _cartService = cartService;
            _orderItemRepository = orderItemRepository;
            _authenticationManager = authenticationManager;
            _productService = productService;
        }

        public int Create(List<CartModel> cardItems)
        {
            Order order = AddOrder();

            foreach (var item in cardItems)
            {
                AddOrderItem(order, item);

                UpdateCart(item);

                UpdateProduct(item);
            }

            _unitOfWork.Commit();

            return order.Id;
        }

        private Order AddOrder()
        {
            var order = new Order
            {
                AddUserId = _authenticationManager.GetLoggedUserId()
            };

            _orderRepository.Add(order);

            return order;
        }

        private void AddOrderItem(Order order, CartModel item)
        {
            var orderItem = new OrderItem
            {
                OrderId = order.Id,
                ProductId = item.ProductId,
                Price = item.Price,
                Count = item.Count,
                AddUserId = _authenticationManager.GetLoggedUserId()
            };

            _orderItemRepository.Add(orderItem);
        }
        private void UpdateCart(CartModel item)
        {
            var cart = _cartService.Find(item.Id);
            cart.IsRealize = true;

            _cartService.Update(cart);
        }

        private void UpdateProduct(CartModel item)
        {
            var prodcut = _productService.Find(item.ProductId);
            prodcut.Count -= item.Count;
            _productService.Update(prodcut);
        }
    }
}
