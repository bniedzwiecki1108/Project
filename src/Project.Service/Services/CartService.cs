﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Project.Core.Services;
using Project.Core.UnitOfWork;
using Project.Data.Entities;
using Project.Data.Models;
using Project.Repository.Repositories;
using Project.Service.Infrastructure.Authorization;
using System.Collections.Generic;
using System.Linq;

namespace Project.Service.Services
{
    public interface ICartService : IEntityService<Cart>
    {
        List<CartModel> GetCartItems();

        Cart Create(ProductModel cartModel);

        void Delete(int id);
    }

    public class CartService : EntityService<Cart>, ICartService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICartRepository _cartRepository;
        private readonly IMapper _mapper;
        private readonly IAuthenticationManager _authenticationManager;

        public CartService(
            IUnitOfWork unitOfWork,
            ICartRepository cartRepository,
            IMapper mapper,
            IAuthenticationManager authenticationManager)
                : base(unitOfWork, cartRepository)
        {
            _unitOfWork = unitOfWork;
            _cartRepository = cartRepository;
            _mapper = mapper;
            _authenticationManager = authenticationManager;
        }

        public Cart Create(ProductModel cartModel)
        {
            var cart = new Cart
            {
                Count = cartModel.Count,
                IsRealize = false,
                ProductId = cartModel.Id,
                UserId = _authenticationManager.GetLoggedUserId().Value
            };

            Create(cart);

            return cart;
        }       

        public List<CartModel> GetCartItems() {
            var userId =
                _authenticationManager
                    .GetLoggedUserId().Value;

            return _cartRepository
                .Queryable()
                .Where(
                       u => u.UserId == userId
                       && !u.IsRealize)
                .ProjectTo<CartModel>(_mapper.ConfigurationProvider)
            .ToList();
        }

        public void Delete(int id)
        {
            var cart = Find(id);
            Delete(cart);
        }
    }
}
