﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Project.Core.Services;
using Project.Core.UnitOfWork;
using Project.Data.Entities;
using Project.Data.Models;
using Project.Repository.Repositories;
using Project.Service.Infrastructure.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Service.Services
{
    public interface IProductService : IEntityService<Product>
    {
        Product Create(ProductModel productModel, HttpPostedFileBase file);

        IEnumerable<ProductModel> GetProducts();

        IEnumerable<ProductModel> GetProductsBy(List<int> categories);

        ProductModel GetProductBy(int id);
    }

    public class ProductService : EntityService<Product>, IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;
        private readonly IImageService _imageService;
        private readonly IAuthenticationManager _authenticationManager;

        public ProductService(
            IUnitOfWork unitOfWork,
            IProductRepository productRepository,
            IMapper mapper,
            IImageService imageService,
            IAuthenticationManager authenticationManager)
                : base(unitOfWork, productRepository)
        {
            _unitOfWork = unitOfWork;
            _productRepository = productRepository;
            _mapper = mapper;
            _imageService = imageService;
            _authenticationManager = authenticationManager;
        }

        public Product Create(ProductModel productModel, HttpPostedFileBase file)
        {
            var product = AddProduct(productModel);

            AddImage(file, product);

            _unitOfWork.Commit();

            return product;
        }

        private Product AddProduct(ProductModel productModel) =>
            _productRepository.
                Add(new Product
                {
                    Name = productModel.Name,
                    Price = productModel.Price,
                    Description = productModel.Description,
                    Count = productModel.Count,
                    CategoryId = productModel.CategoryId,
                    AddUserId = _authenticationManager.GetLoggedUserId()
                });

        private void AddImage(HttpPostedFileBase file, Product product)
        {
            var guid = Guid.NewGuid();
            var extension = "." + file.FileName.Split('.').LastOrDefault();

            var path = _imageService.Write(file, guid, extension);
            var pathResize = _imageService.WriteResize(file, guid, extension);

            _imageService.Add(new Image
            {
                Path = path,
                PathResize = pathResize,
                Name = guid.ToString(),
                Extension = extension,
                ProductId = product.Id,
                AddUserId = _authenticationManager.GetLoggedUserId()
            });
        }

        public IEnumerable<ProductModel> GetProducts() =>
            _productRepository
                .Queryable()            
                .ProjectTo<ProductModel>(_mapper.ConfigurationProvider)
                .ToList();
            

        public IEnumerable<ProductModel> GetProductsBy(List<int> categories) =>
            _productRepository
                .Queryable()
                .Where(c => categories.Contains(c.CategoryId))
                .ProjectTo<ProductModel>(_mapper.ConfigurationProvider)
                .ToList();

        public ProductModel GetProductBy(int id) =>
            _productRepository
                .Queryable()
                .Where( i => i.Id == id)
                .ProjectTo<ProductModel>(_mapper.ConfigurationProvider)
                .FirstOrDefault();
    }
}
