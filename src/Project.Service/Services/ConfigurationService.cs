﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Project.Core.Services;
using Project.Core.UnitOfWork;
using Project.Data.Entities;
using Project.Data.Models;
using Project.Repository.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Project.Service.Services
{
    public interface IConfigurationService : IEntityService<Configuration>
    {
        IEnumerable<ConfigurationModel> GetConfigurations();
    }

    public class ConfigurationService : EntityService<Configuration>, IConfigurationService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfigurationRepository _configurationRepository;
        private readonly IMapper _mapper;

        public ConfigurationService(
            IUnitOfWork unitOfWork,
            IConfigurationRepository configurationRepository,
            IMapper mapper)
                : base(unitOfWork, configurationRepository)
        {
            _unitOfWork = unitOfWork;
            _configurationRepository = configurationRepository;
            _mapper = mapper;
        }

        public IEnumerable<ConfigurationModel> GetConfigurations() =>
            _configurationRepository
                .GetConfigurations()
                .ProjectTo<ConfigurationModel>(_mapper.ConfigurationProvider)
                .ToList();
    }
}
