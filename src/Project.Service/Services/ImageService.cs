﻿using AutoMapper;
using ImageResizer;
using Project.Core.Services;
using Project.Core.UnitOfWork;
using Project.Data.Entities;
using Project.Repository.Repositories;
using Project.Service.Settings;
using System;
using System.Linq;
using System.Web;

namespace Project.Service.Services
{
    public interface IImageService : IEntityService<Image>
    {
        string Write(HttpPostedFileBase image, Guid guid, string extension);

        string WriteResize(HttpPostedFileBase file, Guid guid, string extension);

        void Add(Image image);

        Image GetImageBy(int productId);
    }

    public class ImageService : EntityService<Image>, IImageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IImageRepository _imageRepository;
        private readonly IMapper _mapper;
        private readonly ISettingsManager _settingsManager;

        public ImageService(
            IUnitOfWork unitOfWork,
            IImageRepository imageRepository,
            IMapper mapper,
            ISettingsManager settingsManager)
                : base(unitOfWork, imageRepository)
        {
            _unitOfWork = unitOfWork;
            _imageRepository = imageRepository;
            _mapper = mapper;
            _settingsManager = settingsManager;
        }

        public string Write(HttpPostedFileBase image, Guid guid, string extension)
        {
            var path = _settingsManager.ImagePath + guid + extension;
            image.SaveAs(path);

            return path;
        }

        public string WriteResize(HttpPostedFileBase file, Guid guid, string extension)
        {
            ResizeSettings resizeSetting = new ResizeSettings
            {
                Width = 600,
                Height = 336
            };

            var path = _settingsManager.ImagePathResize + guid + extension;

            ImageBuilder.Current.Build(file, path, resizeSetting);

            return path;
        }

        public void Add(Image image) =>
            _imageRepository.Add(image);

        public Image GetImageBy(int productId) =>
            Queryable()
                .Where(p => p.ProductId == productId)
                .FirstOrDefault();
    }
}
