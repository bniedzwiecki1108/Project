﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Project.Core.Services;
using Project.Core.UnitOfWork;
using Project.Data.Entities;
using Project.Data.Models;
using Project.Repository.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Project.Service.Services
{
    public interface ICategoryService : IEntityService<Category>
    {
        List<CategoryModel> GetCategories();

        List<int> GetChildrenCategoriesId(List<CategoryModel> allCategories, int value);
    }

    public class CategoryService : EntityService<Category>, ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;

        public CategoryService(
            IUnitOfWork unitOfWork,
            ICategoryRepository categoryRepository,
            IMapper mapper)
                : base(unitOfWork, categoryRepository)
        {
            _unitOfWork = unitOfWork;
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public List<CategoryModel> GetCategories() =>
            _categoryRepository
                .Queryable()
                .ProjectTo<CategoryModel>(_mapper.ConfigurationProvider)
                .ToList();

        public List<int> GetChildrenCategoriesId(List<CategoryModel> allCategories, int parentId)
        {
            List<int> categoriesId = new List<int>();
            foreach (var t in allCategories.Where(item => item.ParentId == parentId))
            {
                categoriesId.Add(t.Id.Value);
                categoriesId = categoriesId.Union(GetChildrenCategoriesId(allCategories, t.Id.Value)).ToList();
            }

            return categoriesId;
        }
    }
}
