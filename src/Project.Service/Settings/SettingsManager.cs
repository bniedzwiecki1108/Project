﻿using Project.Service.Infrastructure.Authorization;

namespace Project.Service.Settings
{
    public interface ISettingsManager
    {
        string Salt { get; }

        string ImagePath { get; }

        string ImagePathResize { get; }
    }

    public class SettingsManager : ISettingsManager
    {
        private readonly IMemoryCasheService _memoryCasheService;

        public SettingsManager(IMemoryCasheService memoryCasheService)
        {
            _memoryCasheService = memoryCasheService;
        }

        public string Salt => _memoryCasheService.Get(nameof(Salt));

        public string ImagePath => _memoryCasheService.Get(nameof(ImagePath));

        public string ImagePathResize => _memoryCasheService.Get(nameof(ImagePathResize));
    }
}
