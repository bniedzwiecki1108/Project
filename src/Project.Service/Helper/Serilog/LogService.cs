﻿using Project.Service.Infrastructure.Authorization;
using Project.Service.Settings;
using Serilog;
using Serilog.Sinks.MSSqlServer;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;

namespace Project.Service.Helper.Serilog
{
    public interface ILogService
    {
        ILogger Logger { get; }
    }

    public class LogService : ILogService
    {
        private readonly ILogger Logger;
        private readonly string ConnectionString = ConfigurationManager.ConnectionStrings["ProjectContext"].ToString();
        private readonly IAuthenticationManager _authenticationManager;

        ILogger ILogService.Logger { get => Logger; }

        public LogService(IAuthenticationManager authenticationManager)
        {
            _authenticationManager = authenticationManager;

            ColumnOptions columnOption = GetColumnOption();

            Logger = new LoggerConfiguration()
                .WriteTo.MSSqlServer(ConnectionString, "Serilog", columnOptions: columnOption)
                .CreateLogger()
                .ForContext("UserId", _authenticationManager.GetLoggedUserId());
        }

        private static ColumnOptions GetColumnOption()
        {
            var columnOption = new ColumnOptions();
            columnOption.AdditionalDataColumns = new Collection<DataColumn>
            {
                new DataColumn {DataType = typeof (int), ColumnName = "UserId"},
            };
            return columnOption;
        }
    }
}
