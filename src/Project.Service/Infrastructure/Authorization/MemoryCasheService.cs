﻿using System;
using System.Runtime.Caching;

namespace Project.Service.Infrastructure.Authorization
{
    public interface IMemoryCasheService
    {
        void Set(string key, object value);

        string Get(string key);
    }

    public class MemoryCasheService : IMemoryCasheService
    {
        private MemoryCache cache;

        public MemoryCasheService()
        {
            cache = MemoryCache.Default;
        }

        public void Set(string key, object value)
        {
            var cachePolicty = new CacheItemPolicy();
            cachePolicty.AbsoluteExpiration = DateTime.Now.AddYears(1);

            cache.Set(key, value, cachePolicty);
        }

        public string Get(string key) =>
            cache.Get(key).ToString();
    }
}

