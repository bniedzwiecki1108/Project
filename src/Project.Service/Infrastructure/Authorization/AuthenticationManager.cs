﻿using Newtonsoft.Json;
using Project.Data.Entities;
using Project.Service.Settings;
using System;
using System.Security.Cryptography;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace Project.Service.Infrastructure.Authorization
{
    public interface IAuthenticationManager
    {
        string HashPassword(string password);

        bool IsPasswordConfirmed(string hashPassword, string password);

        void SetAuthenticationToken(User user);

        User GetUserData();

        void Logout();

        int? GetLoggedUserId();
    }

    public class AuthenticationManager : IAuthenticationManager
    {
        private readonly ISettingsManager _settingsManager;

        public AuthenticationManager(ISettingsManager settingsManager)
        {
            _settingsManager = settingsManager;
        }

        public string HashPassword(string password)
        {
            string salt = _settingsManager.Salt;
            byte[] data = System.Text.Encoding.ASCII.GetBytes(salt + password);
            data = MD5.Create().ComputeHash(data);
            return Convert.ToBase64String(data);
        }

        public bool IsPasswordConfirmed(string password, string hashPassword) =>
            HashPassword(password) == hashPassword;

        public void SetAuthenticationToken(User user)
        {
            var userData = JsonConvert.SerializeObject(user,Formatting.Indented, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });

            var ticket = new FormsAuthenticationTicket(
                1,
                user.Name,
                DateTime.Now,
                DateTime.Now.AddMinutes(20),
                false,
                userData);

            string cookieData = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookieData)
            {
                HttpOnly = true,
                Expires = ticket.Expiration
            };

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public User GetUserData()
        {
            User userData = null;
            if (HttpContext.Current != null)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                if (cookie != null)
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

                    userData = new JavaScriptSerializer().Deserialize(ticket.UserData, typeof(User)) as User;
                }
            }
            return userData;
        }

        public void Logout() =>
            FormsAuthentication.SignOut();

        public int? GetLoggedUserId() =>
            GetUserData()?.Id;
    }
}