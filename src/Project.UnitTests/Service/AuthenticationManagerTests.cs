﻿using Moq;
using Project.Data.Entities;
using Project.Service.Infrastructure.Authorization;
using Project.Service.Settings;
using Shouldly;
using System.Linq;
using Xunit;

namespace Project.UnitTests.Service
{
    public class AuthenticationManagerTests
    {
        [Fact]
        public void HashPassword_OnCorrectPassword_ReturnTrue()
        {
            //Arrange
            string password = "pass";
            string hashPassword = "KojaF5unmddn1ZC76l9KZA==";

            //Act
            var settingsManagerMock = new Mock<ISettingsManager>();
            settingsManagerMock
                .Setup(s => s.Salt)
                .Returns(ConfigurationData.Data.Where(s => s.Key == "Salt").Select(v => v.Value).FirstOrDefault());

            var sut = new AuthenticationManager(settingsManagerMock.Object);

            var result = sut.HashPassword(password);

            //Assert
            result.ShouldBe(hashPassword);
        }
    }
}
