﻿using Project.Core.Repositories;
using Project.Data.Entities;
using System.Data.Entity;

namespace Project.Repository.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }

    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
