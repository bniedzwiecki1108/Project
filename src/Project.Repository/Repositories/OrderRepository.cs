﻿using Project.Core.Repositories;
using Project.Data.Entities;
using System.Data.Entity;

namespace Project.Repository.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {        
    }
    
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
