﻿using Project.Core.Repositories;
using Project.Data.Entities;
using System.Data.Entity;

namespace Project.Repository.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
    }

    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
