﻿using Project.Core.Repositories;
using Project.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Project.Repositories.Repository
{
    public interface IUserRepository : IRepository<User>
    {
    }

    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }
    }
}
