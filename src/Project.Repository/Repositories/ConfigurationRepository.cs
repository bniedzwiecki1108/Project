﻿using Project.Core.Repositories;
using Project.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Project.Repository.Repositories
{
    public interface IConfigurationRepository : IRepository<Configuration>
    {
        IQueryable<Configuration> GetConfigurations();
    }

    public class ConfigurationRepository : Repository<Configuration>, IConfigurationRepository
    {
        public ConfigurationRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<Configuration> GetConfigurations() =>
            Queryable()
                .Where(c => !c.IsDeleted);
    }
}
