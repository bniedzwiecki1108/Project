﻿using Project.Core.Repositories;
using Project.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Project.Repository.Repositories
{
    public interface ICartRepository : IRepository<Cart>
    {
    }

    public class CartRepository : Repository<Cart>, ICartRepository
    {
        public CartRepository(DbContext dbContext) : base(dbContext)
        {
        }                
    }
}
