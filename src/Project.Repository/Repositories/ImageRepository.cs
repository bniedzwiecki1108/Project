﻿using Project.Core.Repositories;
using Project.Data.Entities;
using System.Data.Entity;

namespace Project.Repository.Repositories
{
    public interface IImageRepository : IRepository<Image>
    {
    }

    public class ImageRepository : Repository<Image>, IImageRepository
    {
        public ImageRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
