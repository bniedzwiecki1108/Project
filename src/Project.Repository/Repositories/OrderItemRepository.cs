﻿using Project.Core.Repositories;
using Project.Data.Entities;
using System.Data.Entity;

namespace Project.Repository.Repositories
{
    public interface IOrderItemRepository : IRepository<OrderItem>
    {
    }

    public class OrderItemRepository : Repository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
