﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Project.Core.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected DbContext _entities;
        protected readonly IDbSet<TEntity> _dbset;

        public Repository(DbContext context)
        {
            _entities = context;
            _dbset = context.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> Queryable() =>
            _dbset;

        public virtual TEntity Find(params object[] keyValues) =>
            _dbset.Find(keyValues);

        public virtual TEntity Add(TEntity entity) =>
            _dbset.Add(entity);

        public virtual TEntity Delete(TEntity entity) =>
            _dbset.Remove(entity);

        public virtual void Edit(TEntity entity) =>
            _entities.Entry(entity).State = EntityState.Modified;

        public virtual void Save() =>
            _entities.SaveChanges();
    }
}
