﻿using Project.Core.Repositories;
using Project.Core.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Project.Core.Services
{
    public abstract class EntityService<TEntity> : IEntityService<TEntity> where TEntity : class
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<TEntity> _repository;

        public EntityService(
            IUnitOfWork unitOfWork,
            IRepository<TEntity> repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public virtual void Create(TEntity entity)
        {
            IsEntityNull(entity);

            _repository.Add(entity);
            _unitOfWork.Commit();
        }

        public virtual void Delete(TEntity entity)
        {
            IsEntityNull(entity);

            _repository.Delete(entity);
            _unitOfWork.Commit();
        }        

        public void Update(TEntity entity)
        {
            IsEntityNull(entity);

            _repository.Edit(entity);
            _unitOfWork.Commit();
        }

        private void IsEntityNull(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
        }

        public IEnumerable<TEntity> Queryable() =>
            _repository.Queryable();

        public TEntity Find(params object[] keyValues)
        {
            return _repository.Find(keyValues);
        }
    }
}
