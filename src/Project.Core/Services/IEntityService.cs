﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Project.Core.Services
{
    public interface IEntityService<TEntity> where TEntity : class
    {
        void Create(TEntity entity);

        void Delete(TEntity entity);

        IEnumerable<TEntity> Queryable();

        void Update(TEntity entity);

        TEntity Find(params object[] keyValues);
    }
}
